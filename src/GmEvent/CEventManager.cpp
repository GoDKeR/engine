/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "CEventManager.hpp"

////////////////////////////////////////////////////////////
/// Namespace
////////////////////////////////////////////////////////////
using namespace Ghrum;

////////////////////////////////////////////////////////////
/// CEventManager::CEventManager
////////////////////////////////////////////////////////////
CEventManager::CEventManager(const PluginKey &owner, TaskManager &scheduler)
    : m_TaskManager(scheduler),
      m_Owner(owner) {
}

////////////////////////////////////////////////////////////
/// CEventManager::postEvent
////////////////////////////////////////////////////////////
void CEventManager::postEvent(Event &event, EventID id) const {
    auto iterator = m_Handlers.find(id);
    if (iterator != m_Handlers.end())
        iterator->second->callEvent(event);
}

////////////////////////////////////////////////////////////
/// CEventManager::postEventAsync
////////////////////////////////////////////////////////////
void CEventManager::postEventAsync(std::shared_ptr<Event> event,
                                   EventID id) const {
    m_TaskManager.asyncTask(
        m_Owner,
        std::bind(&CEventManager::postEvent, this, *event, id));
}

////////////////////////////////////////////////////////////
/// CEventManager::postEventAsync
////////////////////////////////////////////////////////////
void CEventManager::postEventAsync(std::shared_ptr<Event> event,
                                   EventExecutor executor,
                                   EventID id) const {
    auto pFunction = [&] () {
        postEvent(*event, id);
        executor(*event);
    };
    m_TaskManager.asyncTask(m_Owner, pFunction);
}

////////////////////////////////////////////////////////////
/// CEventManager::remove
////////////////////////////////////////////////////////////
void CEventManager::remove(const PluginKey &owner) {
    // =================== Lock ===================
    std::lock_guard<std::mutex> lock(mutex);
    // =================== Lock ===================

    // Check if the user is trying to remove an invalid
    // event.
    const auto &pIterator = m_Plugins.find(owner.getValue());
    if (pIterator == std::end(m_Plugins)) {
        return;
    }

    // Remove every event owned by the given plugin.
    auto &pTuple = pIterator->second;
    for (const auto & node : pTuple) {
        const auto &pHandle = m_Handlers[std::get<2>(node)];
        pHandle->unregisterEvent(std::get<0>(node), std::get<1>(node));
    }
    m_Plugins.erase(pIterator);
}
////////////////////////////////////////////////////////////
/// CEventManager::removeAll
////////////////////////////////////////////////////////////
void CEventManager::removeAll() {
    // =================== Lock ===================
    std::lock_guard<std::mutex> lock(mutex);
    // =================== Lock ===================

    m_Plugins.clear();
    m_Handlers.clear();
}

////////////////////////////////////////////////////////////
/// CEventManager::registerEvent
////////////////////////////////////////////////////////////
EventKey CEventManager::registerEvent(const PluginKey &owner,
                                      EventExecutor executor, EventPriority priority, EventID id) {
    // =================== Lock ===================
    std::lock_guard<std::mutex> lock(mutex);
    // =================== Lock ===================

    const auto &pHandler = m_Handlers[id];
    const auto  pKey     = pHandler->registerEvent(executor, priority);
    if (pKey.isValid()) {
        m_Plugins[owner.getValue()].emplace_back(pKey, priority, id);
    }
    return pKey;
}

////////////////////////////////////////////////////////////
/// CEventManager::unregisterEvent
////////////////////////////////////////////////////////////
void CEventManager::unregisterEvent(const PluginKey &owner, EventID id,
                                    EventKey key) {
    // =================== Lock ===================
    std::lock_guard<std::mutex> lock(mutex);
    // =================== Lock ===================

    const auto &pHandlers = m_Handlers.find(id);
    if (pHandlers == std::end(m_Handlers))
        return;

    auto &pHandler = pHandlers->second;
    auto &pVector  = m_Plugins[owner.getValue()];

    const auto &pPlugins = std::find_if(std::begin(pVector), std::end(pVector),
    [&](const Executor & executor) {
        return std::get<0>(executor) == key && std::get<2>(executor) == id;
    });

    bool isRemoved = pHandler->unregisterEvent(key, std::get<1>(*pPlugins));
    if (!isRemoved)
        return;

    bool isEmpty   = pHandler->isEmpty();
    if (isEmpty)
        m_Handlers.erase(pHandlers);
    pVector.erase(pPlugins);
}