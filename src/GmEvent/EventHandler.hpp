/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _EVENT_HANDLER_HPP_
#define _EVENT_HANDLER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <GmEvent/EventManager.hpp>
#include <GmCore/Handle/HandleTracker.hpp>
#include <algorithm>
#include <vector>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a list of functions.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class EventHandler {
public:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef std::tuple<EventKey, EventExecutor> Node;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Return if the handler is empty.
    ///
    /// \return If the handler is empty.
    ////////////////////////////////////////////////////////////
    inline bool isEmpty()  const {
        for (size_t i = 0; i <= EventPriority::MONITOR; i++) {
            if (m_Executors[i].size() > 0)
                return false;
        }
        return true;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Propagate an event though all functions.
    ///
    /// \param[inout] event The event to propagate
    ////////////////////////////////////////////////////////////
    inline void callEvent(Event &event) const {
        for (size_t i = 0; i <= EventPriority::MONITOR; i++)
            for (const auto & executor : m_Executors[i])
                std::get<1>(executor)(event);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Registers the specified executor.
    ///
    /// \param[in] executor The event's executor.
    /// \param[in] priority The event's priority.
    ///
    /// \return An unique id that represent the executor.
    ////////////////////////////////////////////////////////////
    inline EventKey registerEvent(EventExecutor executor, EventPriority priority)  {
        auto pKey = m_Handles.getHandle();
        if (pKey.isValid()) {
            m_Executors[priority].emplace_back(pKey, executor);
        }
        return pKey;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Unregisters the specified executor.
    ///
    /// \param[in] executor The event's executor id.
    /// \param[in] priority The event's priority.
    ///
    /// \return If the function succeed.
    ////////////////////////////////////////////////////////////
    inline bool unregisterEvent(EventKey executor, EventPriority priority) {
        auto &pArray = m_Executors[priority];
        auto iterator = std::find_if(pArray.begin(), pArray.end(),
        [&](const Node & value) {
            return std::get<0>(value) == executor;
        });
        bool isValid = (iterator != pArray.end());
        if (isValid) {
            m_Handles.releaseHandle(executor);
            pArray.erase(iterator);
        }
        return isValid;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    HandleTracker<EventKey::Type> m_Handles;
    std::vector<Node> m_Executors[EventPriority::MONITOR + 1];
};

}; // namespace Ghrum

#endif // _EVENT_HANDLER_HPP_