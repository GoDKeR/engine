/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _CEVENT_MANAGER_HPP_
#define _CEVENT_MANAGER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "EventHandler.hpp"
#include <GmScheduler/TaskManager.hpp>
#include <unordered_map>
#include <mutex>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Common implementation for EventManager.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class CEventManager : public EventManager {
private:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef std::tuple<EventKey, EventPriority, EventID> Executor;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] owner       The owner of the manager.
    /// \param[in] taskManager The manager for async events.
    ////////////////////////////////////////////////////////////
    CEventManager(const PluginKey &owner, TaskManager &taskManager);

    ////////////////////////////////////////////////////////////
    /// \copydoc EventManager::remove(const PluginKey &)
    ////////////////////////////////////////////////////////////
    void remove(const PluginKey &owner) override;

    ////////////////////////////////////////////////////////////
    /// \copydoc EventManager::removeAll()
    ////////////////////////////////////////////////////////////
    void removeAll() override;
private:
    ////////////////////////////////////////////////////////////
    /// \copydoc EventManager::postEvent(
    ///             Event &,
    ///             uint32_t)
    ////////////////////////////////////////////////////////////
    void postEvent(Event &event, EventID id) const override;

    ////////////////////////////////////////////////////////////
    /// \copydoc EventManager::postEventAsync(
    ///             std::shared_ptr<Event>,
    ///             uint32_t)
    ////////////////////////////////////////////////////////////
    void postEventAsync(std::shared_ptr<Event> event, EventID id) const override;

    ////////////////////////////////////////////////////////////
    /// \copydoc EventManager::postEventAsync(
    ///             std::shared_ptr<Event>,
    ///             EventExecutor,
    ///             uint32_t)
    ////////////////////////////////////////////////////////////
    void postEventAsync(std::shared_ptr<Event> event, EventExecutor executor,
                        EventID id) const override;

    ////////////////////////////////////////////////////////////
    /// \copydoc EventManager::registerEvent(
    ///             const PluginKey &,
    ///             EventExecutor,
    ///             EventPriority,
    ///             uint32_t)
    ////////////////////////////////////////////////////////////
    EventKey registerEvent(const PluginKey &owner, EventExecutor executor,
                           EventPriority priority, EventID id) override;

    ////////////////////////////////////////////////////////////
    /// \copydoc EventManager::unregisterEvent(
    ///             const PluginKey &,
    ///             uint32_t,
    ///             EventKey)
    ////////////////////////////////////////////////////////////
    void unregisterEvent(const PluginKey &owner, EventID id,
                         EventKey key) override;
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::mutex mutex;
    std::unordered_map<PluginKey::Type, std::vector<Executor>> m_Plugins;
    std::unordered_map<EventID, std::unique_ptr<EventHandler>> m_Handlers;
    TaskManager     &m_TaskManager;
    const PluginKey &m_Owner;
};

} // namespace Ghrum

#endif // _CEVENT_MANAGER_HPP_