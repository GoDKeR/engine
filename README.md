[![Project Logo][Project Logo]][Project Site]

## What is Ghrum? 
Ghrum is an upcoming C++ game platform. Read more about Ghrum [here][Ghrum Wiki]

Copyright (c) 2013, Ghrum Inc <<http://www.ghrum.org/>>

## License
Ghrum is licensed under [Apache License, Version 2.0][Project License], Please see the `LICENSE.txt` file for details.

## Contributing
Ghrum is 100% free and open-source. We encourage and support an active community that accepts contributions from the public. We'd like you to be a part of that community.

## Dependency
* Ghrum requires GCC >=4.7.0 to compile.

### Before Contributing to Ghrum please:
* 1. Make sure you're familiar with the aim of Ghrum so we don't end up sitting on code that serves no purpose, read more on that [here][GhrumWiki]
* 2. Read and sign the [**Ghrum Inc. Contributor License Agreement**][Ghrum CLA], to  to confirm you've read and acknowledged the legal aspects of your contributions
* 3. Make sure your code conforms to the formatting guidelines below
* 4. If you have any questions come and ask on #Ghrum @ irc.esper.net

### Coding and Pull Request Formatting
* Generally follow the C++11 coding standards.
* Follow Java formatting.
* Use spaces, no tabs.
* No trailing whitespaces.
* 80 column limit for readability.
* Pull requests must compile, work, and be formatted correctly.
* Sign-off on ALL your commits - this indicates you agree to the terms of our license.
* No merges should be included in pull requests unless the pull request's purpose is a merge.
* Number of commits in a pull request should be kept to *one commit* and all additional commits must be *squashed*.

**Please follow the above conventions if you want your pull request(s) accepted.**

[Project Logo]: http://ghrum.org/styles/core/xenforo/logo.png
[Project License]: https://github.com/Wolftein/Ghrum/LICENSE.txt
[Project Site]: http://ghrum.org

[Ghrum Github]: https://github.com/Wolftein/Ghrum
[Ghrum Wiki]: https://github.com/Wolftein/Ghrum/wiki
[Ghrum CLA]: https://docs.google.com/forms/d/1uTJ-oy4b2n7r-qBCnLjupuy8WKUET_wmwyaldRc9A7Y/viewform
[Ghrum Logo]: http://ghrum.org/styles/core/xenforo/logo.png
