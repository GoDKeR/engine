/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ASSET_LOADER_HPP_
#define _ASSET_LOADER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Asset.hpp"
#include "AssetKey.hpp"
#include "AssetInputStream.hpp"
#include <memory>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief The interface for loading an asset from an input
///        stream.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class AssetLoader {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(AssetLoader);

    ////////////////////////////////////////////////////////////
    /// \brief Parser the asset from an input stream.
    ///
    /// \param[in]  input    The input-stream to read parse from
    /// \param[out] resource The pointer to the resource
    ///
    /// \return True if everything went well, otherwise false
    ////////////////////////////////////////////////////////////
    virtual bool parse(std::shared_ptr<AssetInputStream> input,
                       Asset *resource) const = 0;
};

} // namespace Ghrum

#endif // _ASSET_LOADER_HPP_