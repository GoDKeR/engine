/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ASSET_CACHE_HPP_
#define _ASSET_CACHE_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Asset.hpp"
#include "AssetKey.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define an interface for cache resources.
///
/// Allowing storage of loaded resources in order to improve
/// their access time if they are requested again in a short
/// period of time. Depending on the asset type and how it
/// is used, a specialized caching method can be selected
/// that is most appropriate for that resource type.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class AssetCache {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Adds a resource to the cache.
    ///
    /// \param[in] key The key of the resource
    ///
    /// \return A new reference of the resource in the cache
    ////////////////////////////////////////////////////////////
    virtual Asset &addAsset(const AssetKey &key) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Retrieves a resource from the cache.
    ///
    /// \param[in] key The key of the resource
    ///
    /// \return The resource that was previously cached, or
    ///         null if not found
    ////////////////////////////////////////////////////////////
    virtual Asset *const getAsset(const AssetKey &key) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Removes a resource from the cache memory.
    ///
    /// \param[in] key The key of the resource
    ////////////////////////////////////////////////////////////
    virtual void removeAsset(const AssetKey &key) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Clears the cache memory.
    ////////////////////////////////////////////////////////////
    virtual void clear() = 0;
};

} // namespace Ghrum

#endif // _ASSET_CACHE_HPP_