/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ASSET_LOCATOR_HPP_
#define _ASSET_LOCATOR_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "AssetKey.hpp"
#include "AssetInputStream.hpp"
#include <memory>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Interface for retrieving an input-stream of an
///        asset.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class AssetLocator {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] path The path of the locator
    ////////////////////////////////////////////////////////////
    AssetLocator(const std::string &path)
        : m_Path(path) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(AssetLocator);

    ////////////////////////////////////////////////////////////
    /// \brief Gets the locator's path.
    ///
    /// \return The locator's path
    ////////////////////////////////////////////////////////////
    inline const std::string &getPath() const {
        return m_Path;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Retrieves the input stream of an asset.
    ///
    /// \param[in]  key The assets key information
    ///
    /// \return A shared pointer of the input stream, otherwise
    ///         nullptr
    ////////////////////////////////////////////////////////////
    virtual std::shared_ptr<AssetInputStream> load(const AssetKey &key) const = 0;
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const std::string &m_Path;
};

} // namespace Ghrum

#endif // _ASSET_LOCATOR_HPP_