/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ASSET_KEY_HPP_
#define _ASSET_KEY_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a key handler for the resources.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class AssetKey {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] name   The name of the key
    /// \param[in] isFile True if the asset is a file
    ////////////////////////////////////////////////////////////
    AssetKey(const std::string &name, bool isFile)
        : m_Name(name),
          m_Folder(getFolder(name)),
          m_Extension(getExtension(name)),
          m_IsFile(isFile) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the name of the key.
    ///
    /// \return The name of the key
    ////////////////////////////////////////////////////////////
    inline const std::string &getName() const {
        return m_Name;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the folder of the key.
    ///
    /// \return The folder of the key
    ////////////////////////////////////////////////////////////
    inline const std::string &getFolder() const {
        return m_Folder;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the extension of the key.
    ///
    /// \return The extension of the key
    ////////////////////////////////////////////////////////////
    inline const std::string &getExtension() const {
        return m_Extension;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns if resource is a file.
    ///
    /// \return If resource is a file
    ////////////////////////////////////////////////////////////
    inline bool isFile() const {
        return m_IsFile;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns if resource is a folder.
    ///
    /// \return If resource is a folder
    ////////////////////////////////////////////////////////////
    inline bool isFolder() const {
        return !m_IsFile;
    }
protected:
    ////////////////////////////////////////////////////////////
    /// \brief Return the folder of the given path.
    ///
    /// \param[in] path The full path
    ///
    /// \return The folder of the given path
    ////////////////////////////////////////////////////////////
    static const std::string getFolder(const std::string &path) {
        std::string::size_type index = path.find(PATH_SEPARATOR);
        return (index <= 0 || index == path.length() - 1
                ? ""
                : path.substr(index + 1));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Return the extension of the given path.
    ///
    /// \param[in] path The full path
    ///
    /// \return The extension of the given path
    ////////////////////////////////////////////////////////////
    static const std::string getExtension(const std::string &path) {
        std::string::size_type index = path.find('.');
        return (index <= 0 || index == path.length() - 1
                ? ""
                : path.substr(index + 1));
    }
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const std::string m_Name;
    const std::string m_Folder;
    const std::string m_Extension;
    const bool m_IsFile;        
};

} // namespace Ghrum

#endif // _ASSET_KEY_HPP_