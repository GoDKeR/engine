/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _FILE_INPUT_STREAM_HPP_
#define _FILE_INPUT_STREAM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <GmFilesystem/AssetInputStream.hpp>
#include <fstream>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Input stream of a file resource.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class FileInputStream : public AssetInputStream {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] name The name of the file
    ////////////////////////////////////////////////////////////
    FileInputStream(const std::string &name)
        : m_Position(0) {
        // Open the file using read mode only.
        m_Handler = std::fopen(name.c_str(), "r");

        // Get the size of the file, by going to the end
        // of the file.
        fseek(m_Handler, 0, SEEK_END);
        m_Length = ftell(m_Handler);
        rewind(m_Handler);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    ~FileInputStream() {
        close();
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::read(void *, ssize_t)
    ////////////////////////////////////////////////////////////
    ssize_t read(void *buffer, ssize_t size) override {
        return std::fread(buffer, sizeof(uint8_t), size, m_Handler);
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::seek(ssize_t)
    ////////////////////////////////////////////////////////////
    ssize_t seek(ssize_t position) override {
        if (std::fseek(m_Handler, position, SEEK_CUR) == 0) {
            m_Position += position;
        }
        return m_Position;
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::position()
    ////////////////////////////////////////////////////////////
    ssize_t position() const override {
        return m_Position;
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::length()
    ////////////////////////////////////////////////////////////
    ssize_t length() const override {
        return m_Length;
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::close()
    ////////////////////////////////////////////////////////////
    void close() override {
        if (m_Handler != nullptr) fclose(m_Handler);
        m_Handler = nullptr;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::FILE *m_Handler;
    ssize_t    m_Length, m_Position;
};

} // namespace Ghrum

#endif // _FILE_INPUT_STREAM_HPP_