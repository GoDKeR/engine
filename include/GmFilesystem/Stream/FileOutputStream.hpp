/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _FILE_OUTPUT_STREAM_HPP_
#define _FILE_OUTPUT_STREAM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <GmFilesystem/AssetOutputStream.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Output stream of a file resource.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class FileOutputStream : public AssetOutputStream {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] name The name of the file
    ////////////////////////////////////////////////////////////
    FileOutputStream(const std::string &name)
        : m_Length(0) {
        m_Handler = std::fopen(name.c_str(), "w");
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    ~FileOutputStream() {
        close();
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetOutputStream::write(void *, ssize_t)
    ////////////////////////////////////////////////////////////
    ssize_t write(void *buffer, ssize_t size) override {
        auto pSize = fwrite(buffer, sizeof(uint8_t), size, m_Handler);
        if (pSize == size) {
            m_Length += pSize;
        }
        return pSize;
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetOutputStream::length()
    ////////////////////////////////////////////////////////////
    ssize_t length() const override {
        return m_Length;
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetOutputStream::close()
    ////////////////////////////////////////////////////////////
    void close() override {
        if (m_Handler != nullptr) fclose(m_Handler);
        m_Handler = nullptr;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::FILE *m_Handler;
    size_t     m_Length;
};

} // namespace Ghrum

#endif // _FILE_OUTPUT_STREAM_HPP_