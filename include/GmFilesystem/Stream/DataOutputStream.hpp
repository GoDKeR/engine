/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _DATA_OUTPUT_STREAM_HPP_
#define _DATA_OUTPUT_STREAM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <GmFilesystem/AssetOutputStream.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Generic class for custom data output streams.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class DataOutputStream {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] stream The underlying stream
    ////////////////////////////////////////////////////////////
    DataOutputStream(AssetOutputStream &stream)
        : m_Stream(stream) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] stream The underlying stream
    ////////////////////////////////////////////////////////////
    DataOutputStream(AssetOutputStream &&stream)
        : m_Stream(stream) {
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetOutputStream::write(void *, ssize_t)
    ////////////////////////////////////////////////////////////
    inline ssize_t write(void *buffer, ssize_t size) {
        return m_Stream.write(buffer, size);
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetOutputStream::length()
    ////////////////////////////////////////////////////////////
    inline ssize_t length() const {
        return m_Stream.length();
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetOutputStream::close()
    ////////////////////////////////////////////////////////////
    inline void close() {
        m_Stream.close();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes Any type into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    template<typename T>
    inline void write(T value) {
        write(static_cast<void *>(&value), sizeof(T));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes a boolean into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeBoolean(bool value) {
        writeUnsignedByte(value ? 1 : 0);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed byte into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeByte(int8_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned byte into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeUnsignedByte(uint8_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed short into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeShort(int16_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned short into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeUnsignedShort(uint16_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed integer into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeInteger(int32_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned integer into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeUnsignedInteger(uint32_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed long into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeLong(int64_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned long into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeUnsignedLong(uint64_t value) {
        write(value);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes a float into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeFloat(float value) {
        union {
            uint32_t index;
            float value;
        } ieee_float;
        ieee_float.value = value;
        write(ieee_float.index);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Writes a double into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    inline void writeDouble(double value) {
        union {
            uint64_t index;
            double value;
        } ieee_double;
        ieee_double.value = value;
        write(ieee_double.index);
    }
private:
    ////////////////////////////////////////////////////////////
    /// Member data
    ////////////////////////////////////////////////////////////
    AssetOutputStream &m_Stream;
};

} // namespace Ghrum

#endif // _DATA_OUTPUT_STREAM_HPP_