/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _DATA_INPUT_STREAM_HPP_
#define _DATA_INPUT_STREAM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <GmFilesystem/AssetInputStream.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Generic class for custom data input streams.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class DataInputStream {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] stream The stream of the view stream
    ////////////////////////////////////////////////////////////
    DataInputStream(AssetInputStream &stream)
        : m_Stream(stream) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] stream The stream of the view stream
    ////////////////////////////////////////////////////////////
    DataInputStream(AssetInputStream &&stream)
        : m_Stream(stream) {
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::read(void *, ssize_t)
    ////////////////////////////////////////////////////////////
    inline ssize_t read(void *buffer, ssize_t size) {
        return m_Stream.read(buffer, size);
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::seek(size_t)
    ////////////////////////////////////////////////////////////
    inline ssize_t seek(ssize_t position) {
        return m_Stream.seek(position);
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::position()
    ////////////////////////////////////////////////////////////
    inline ssize_t position() const {
        return m_Stream.position();
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::length()
    ////////////////////////////////////////////////////////////
    inline ssize_t length() const {
        return m_Stream.length();
    }

    ////////////////////////////////////////////////////////////
    /// @copydoc AssetInputStream::close()
    ////////////////////////////////////////////////////////////
    inline void close() {
        m_Stream.close();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Metaprogramming function to read any type.
    ///
    /// \tparam T Template of type Any
    ///
    /// \return Any type from the stream
    ////////////////////////////////////////////////////////////
    template<typename T>
    inline T read() {
        T pValue;
        read(&pValue, sizeof(T));
        return pValue;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads a boolean from the stream.
    ///
    /// \return A boolean from the stream
    ////////////////////////////////////////////////////////////
    inline bool readBoolean() {
        return read<bool>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads a signed byte from the stream.
    ///
    /// \return A signed byte from the stream
    ////////////////////////////////////////////////////////////
    inline int8_t readByte() {
        return read<int8_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned byte from the stream.
    ///
    /// \return An unsigned byte from the stream
    ////////////////////////////////////////////////////////////
    inline uint8_t readUnsignedByte() {
        return read<uint8_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads a signed short from the stream.
    ///
    /// \return A signed short from the stream
    ////////////////////////////////////////////////////////////
    inline int16_t readShort() {
        return read<int16_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned short from the stream.
    ///
    /// \return An unsigned short from the stream
    ////////////////////////////////////////////////////////////
    inline uint16_t readUnsignedShort() {
        return read<uint16_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads a singned integer from the stream.
    ///
    /// \return A singned integer from the stream
    ////////////////////////////////////////////////////////////
    inline int32_t readInteger() {
        return read<int32_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned integer from the stream.
    ///
    /// \return An unsigned integer from the stream
    ////////////////////////////////////////////////////////////
    inline uint32_t readUnsignedInteger() {
        return read<uint32_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads a signed long from the stream.
    ///
    /// \return A signed long from the stream
    ////////////////////////////////////////////////////////////
    inline int64_t readLong() {
        return read<int64_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned long from the stream.
    ///
    /// \return An unsigned long from the stream
    ////////////////////////////////////////////////////////////
    inline uint64_t readUnsignedLong() {
        return read<uint64_t>();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads a float from the stream.
    ///
    /// \return A float from the stream
    ////////////////////////////////////////////////////////////
    inline float readFloat() {
        union {
            uint32_t index;
            float value;
        } ieee_float;
        ieee_float.index = read<uint32_t>();
        return ieee_float.value;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Reads a double from the stream.
    ///
    /// \return A double from the stream
    ////////////////////////////////////////////////////////////
    inline double readDouble() {
        union {
            uint64_t index;
            double value;
        } ieee_double;
        ieee_double.index = read<uint64_t>();
        return ieee_double.value;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    AssetInputStream &m_Stream;
};

} // namespace Ghrum

#endif // _DATA_INPUT_STREAM_HPP_