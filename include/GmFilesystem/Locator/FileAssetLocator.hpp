/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _FILE_ASSET_LOCATOR_HPP_
#define _FILE_ASSET_LOCATOR_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <GmFilesystem/AssetLocator.hpp>
#include <GmFilesystem/Stream/FileInputStream.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a file resource locator.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class FileAssetLocator : public AssetLocator {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] path The path of the locator
    ////////////////////////////////////////////////////////////
    FileAssetLocator(const std::string &path)
        : AssetLocator(path) {
    }

    ////////////////////////////////////////////////////////////
    /// \copydoc AssetLocator::load(const AssetKey &)
    ////////////////////////////////////////////////////////////
    std::shared_ptr<AssetInputStream> load(const AssetKey &key) const override {
        auto doesExist = doesFileExist(m_Path + key.getName());
        return (doesExist == true
                ? std::make_shared<FileInputStream>(m_Path + key.getName())
                : nullptr);
    }
public:
    ////////////////////////////////////////////////////////////
    /// \brief Check if the file exist.
    ///
    /// \param[in] path The path of the locator
    ///
    /// \return True if the file exist, otherwise False
    ////////////////////////////////////////////////////////////
    static bool doesFileExist(const std::string &name) {
        std::FILE *pFile = fopen(name.c_str(), "r");
        if (pFile != nullptr) {
            fclose(pFile);
            return true;
        }
        return false;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
};

} // namespace Ghrum

#endif // _FILE_ASSET_LOCATOR_HPP_