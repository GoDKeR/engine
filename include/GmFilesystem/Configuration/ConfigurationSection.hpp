/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _CONFIGURATION_SECTION_HPP_
#define _CONFIGURATION_SECTION_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>
#include <unordered_map>
#include <vector>
#include <set>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a section of the configuration.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ConfigurationSection {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Gets a set containing all keys in this section.
    ///
    /// If isDeeper is set to true, then this will contain all
    /// keys within any child.
    ///
    /// \param[in] isDeeper Whether or not to get a deeper list
    ///
    /// \return A set of keys contained within this section.
    ////////////////////////////////////////////////////////////
    virtual std::set<std::string> getKeys(bool isDeeper) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Check if this section contains the given path.
    ///
    /// \param[in] path Path to check for existence
    ///
    /// \return True if this section contains the requested path
    ////////////////////////////////////////////////////////////
    virtual bool contains(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the path of the section.
    ///
    /// \return The path of the section from its root
    ////////////////////////////////////////////////////////////
    virtual const std::string &getPath() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the parent section.
    ///
    /// \return The parent section containing this section
    ////////////////////////////////////////////////////////////
    virtual ConfigurationSection &getParent() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Creates an empty section at the specified path.
    ///
    /// Any value that was previously set at this path will be
    /// overwritten.
    ///
    /// \param[in] path Path to create the section at
    ///
    /// \return The newly created section
    ////////////////////////////////////////////////////////////
    virtual ConfigurationSection &createSection(const std::string &path) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested bool by path.
    ///
    /// \param[in] path Path of the value to get
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual bool getBoolean(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested bool by path.
    ///
    /// \param[in] path Path of the value to get
    /// \param[in] def  Default value if the requested value
    ///                 doesn't exist
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual bool getBoolean(const std::string &path, bool def) const = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a bool.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isBoolean(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested string by path.
    ///
    /// \param[in] path Path of the value to get
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual std::string getString(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested string by path.
    ///
    /// \param[in] path Path of the value to get
    /// \param[in] def  Default value if the requested value
    ///                 doesn't exist
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual std::string getString(const std::string &path,
                                  const std::string &def) const = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a string.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isString(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested integer by path.
    ///
    /// \param[in] path Path of the value to get
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual uint32_t getInteger(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested integer by path.
    ///
    /// \param[in] path Path of the value to get
    /// \param[in] def  Default value if the requested value
    ///                 doesn't exist
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual uint32_t getInteger(const std::string &path, uint32_t def) const = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a integer.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isInteger(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested long by path.
    ///
    /// \param[in] path Path of the value to get
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual uint64_t getLong(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested long by path.
    ///
    /// \param[in] path Path of the value to get
    /// \param[in] def  Default value if the requested value
    ///                 doesn't exist
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual uint64_t getLong(const std::string &path, uint64_t def) const = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a long.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isLong(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested double by path.
    ///
    /// \param[in] path Path of the value to get
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual double getDouble(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested double by path.
    ///
    /// \param[in] path Path of the value to get
    /// \param[in] def  Default value if the requested value
    ///                 doesn't exist
    ///
    /// \return The value requested
    ////////////////////////////////////////////////////////////
    virtual double getDouble(const std::string &path, double def) const = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a double.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isDouble(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested section by path.
    ///
    /// \param[in] path Path of the section to get
    ///
    /// \return Requested section or this section if doesn't exist
    ////////////////////////////////////////////////////////////
    virtual ConfigurationSection &getSection(const std::string &path) = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a section.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isConfigurationSection(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a list.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isList(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of string by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a string
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get
    ///
    /// \return The requested list
    ////////////////////////////////////////////////////////////
    virtual std::vector<std::string> getStringList(
        const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of integer by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a integer
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get
    ///
    /// \return The requested list
    ////////////////////////////////////////////////////////////
    virtual std::vector<uint32_t> getIntegerList(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of bool by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a bool
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get
    ///
    /// \return The requested list
    ////////////////////////////////////////////////////////////
    virtual std::vector<bool> getBooleanList(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of double by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a double
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get
    ///
    /// \return The requested list
    ////////////////////////////////////////////////////////////
    virtual std::vector<double> getDoubleList(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of float by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a float
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get
    ///
    /// \return The requested list
    ////////////////////////////////////////////////////////////
    virtual std::vector<float> getFloatList(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of long by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a long
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get
    ///
    /// \return The requested list
    ////////////////////////////////////////////////////////////
    virtual std::vector<uint64_t> getLongList(const std::string &path)  = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of byte by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a byte
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get
    ///
    /// \return The requested list
    ////////////////////////////////////////////////////////////
    virtual std::vector<uint8_t> getByteList(const std::string &path) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested list of short by path.
    ///
    /// If the list does not exist, this will return an empty
    /// list.
    ///
    /// This method will attempt to cast any values into a short
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the list to get.
    ///
    /// \return The requested list.
    ////////////////////////////////////////////////////////////
    virtual std::vector<uint16_t> getShortList(const std::string &path) = 0;

    ////////////////////////////////////////////////////////////
    /// \param Check if the specified path is a map.
    ///
    /// \param[in] path Path of the value to check
    ///
    /// \return Whether or not the specified path is valid
    ////////////////////////////////////////////////////////////
    virtual bool isMap(const std::string &path) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the requested map of string by path.
    ///
    /// If the map does not exist, this will return an empty
    /// map.
    ///
    /// This method will attempt to cast any values into a string
    /// if possible, but may miss any values out if they are not
    /// compatible.
    ///
    /// \param[in] path Path of the map to get
    ///
    /// \return The requested map
    ////////////////////////////////////////////////////////////
    virtual std::unordered_map<std::string, std::string> getStringMap(
        const std::string &path) const = 0;
};

} // namespace Ghrum

#endif // _CONFIGURATION_SECTION_HPP_