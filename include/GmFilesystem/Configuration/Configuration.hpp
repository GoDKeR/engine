/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _CONFIGURATION_HPP_
#define _CONFIGURATION_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "ConfigurationSection.hpp"
#include <GmFilesystem/Asset.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define an interface for a configuration stream.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Configuration : public ConfigurationSection, public Asset {
public:
    ///////////////////////////////////////////s/////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(Configuration);

    ////////////////////////////////////////////////////////////
    /// \brief Saves it to the specified location.
    ///
    /// If the file does not exist, it will be created. If
    /// already exists, it will be overwritten. If it cannot be
    /// be overwritten or created, an exception will be thrown.
    ///
    /// \param[in] filename The configuration's filename
    ////////////////////////////////////////////////////////////
    virtual void save(const std::string &filename) const = 0;
};

} // namespace Ghrum

#endif // _CONFIGURATION_HPP_