/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ASSET_INPUT_STREAM_HPP_
#define _ASSET_INPUT_STREAM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Generic class for custom input streams.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class AssetInputStream {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(AssetInputStream);

    ////////////////////////////////////////////////////////////
    /// \brief Close the stream.
    ////////////////////////////////////////////////////////////
    virtual void close() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Read a number of bytes into a buffer.
    ///
    /// \param[inout] buffer The buffer to hold the bytes
    /// \param[in]    length The number of bytes to read
    ///
    /// \return The number of bytes readed
    ////////////////////////////////////////////////////////////
    virtual ssize_t read(void * buffer, ssize_t length) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Moves the position pointer.
    ///
    /// \param[in] position The new position
    ///
    /// \return The new position of the buffer
    ////////////////////////////////////////////////////////////
    virtual ssize_t seek(ssize_t position) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the current position.
    ///
    /// \return the current position
    ////////////////////////////////////////////////////////////
    virtual ssize_t position() const = 0;
    
    ////////////////////////////////////////////////////////////
    /// \brief Gets the buffer's size.
    ///
    /// \return the buffer's size
    ////////////////////////////////////////////////////////////
    virtual ssize_t length() const = 0;
};

} // namespace Ghrum

#endif // _ASSET_INPUT_STREAM_HPP_