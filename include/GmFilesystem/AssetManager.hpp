/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ASSET_MANAGER_HPP_
#define _ASSET_MANAGER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "AssetLoader.hpp"
#include "AssetLocator.hpp"
#include <vector>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the manager of all resources.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class AssetManager {
public:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef std::function<bool (const AssetKey &)> FileComparator;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(AssetManager);

    ////////////////////////////////////////////////////////////
    /// \brief Gets a resource from the resource manager.
    ///
    /// If the resource doesn't exist, then return a default
    /// resource.
    ///
    /// \param[in] key The key of the resource
    ///
    /// \return The reference of the resource
    ////////////////////////////////////////////////////////////
    virtual Asset &getResource(const AssetKey &key) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Register a @class IResourceLoader.
    ///
    /// \param[in] loader    The resource loader
    /// \param[in] extension The loader's extension
    ////////////////////////////////////////////////////////////
    virtual void registerLoader(std::unique_ptr<AssetLoader> loader,
                                const std::string &extension) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Register a @class IResourceLocator.
    ///
    /// \param[in] locator The resource locator
    ////////////////////////////////////////////////////////////
    virtual void registerLocator(std::unique_ptr<AssetLocator> locator) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Unregister a @class IResourceLocator.
    ///
    /// \param[in] path The path of the locator
    ////////////////////////////////////////////////////////////
    virtual void unregisterLocator(const std::string &path) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Check if the given file exist.
    ///
    /// \param[in] filename    The filename of the resource
    /// \param[in] isAbsoluate True if the path is not from the
    ///                        filesystem itself
    ///
    /// \return True if the file exist, otherwise False
    ////////////////////////////////////////////////////////////
    virtual bool isFileExist(const std::string &filename,
                             bool isAbsolute) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief List every file in the given folder.
    ///
    /// \param[in] folder     Where to look the file files
    /// \param[in] isDeeper   If should check for child folders
    /// \param[in] comparator The function comparator for the
    ///                       search
    ///
    /// \return A vector that contains all files found.
    ////////////////////////////////////////////////////////////
    virtual std::vector<std::string> listFiles(const std::string &folder,
            bool isDeeper,
            FileComparator comparator) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Find the the file and open a stream to read it.
    ///
    /// \param[in]  filename The filename of the resource
    /// \param[out] in       The stream of the file
    /// \return If the file was found
    ////////////////////////////////////////////////////////////
    virtual bool findResource(const std::string &filename,
                              AssetInputStream &in) = 0;
};

} // namespace Ghrum

#endif // _ASSET_MANAGER_HPP_