/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _TASK_EXECUTOR_HPP_
#define _TASK_EXECUTOR_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>
#include <functional>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a way to get function callback easily.
///
/// \param[in] Function The function
////////////////////////////////////////////////////////////
#define getFunctionTask(Function) \
    TaskExecutor(Function)

////////////////////////////////////////////////////////////
/// \brief Define a way to get member callback easily.
///
/// \param[in] Type The type of the member
/// \param[in] Instance The Type's instance
////////////////////////////////////////////////////////////
#define getMemberTask(Type, Instance) \
    TaskExecutor(std::bind(&Type, Instance))

////////////////////////////////////////////////////////////
/// \brief Represents an task's executor.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class TaskExecutor {
public:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef std::function<void ()> Generic;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \tparam T Template of type Function.
    ///
    /// \param[in] executor The function executor
    ////////////////////////////////////////////////////////////
    template<typename T>
    TaskExecutor(T &&executor)
        : m_Function(executor) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \tparam T Template of type Function.
    ///
    /// \param[in] executor The function executor
    ////////////////////////////////////////////////////////////
    template<typename T>
    TaskExecutor(const T &&executor)
        : m_Function(executor) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Call operator.
    ////////////////////////////////////////////////////////////
    inline void operator()() {
        m_Function();
    }
protected:
    ////////////////////////////////////////////////////////////
    /// Member data
    ////////////////////////////////////////////////////////////
    Generic m_Function;
};

} // namespace Ghrum

#endif // _TASK_EXECUTOR_HPP_