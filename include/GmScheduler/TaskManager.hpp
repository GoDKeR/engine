/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _TASK_MANAGER_HPP_
#define _TASK_MANAGER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Task.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represent the task's manager.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class TaskManager {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Schedules a synchronized task.
    ///
    /// The task will be executed by the main thread.
    ///
    /// \param[in] owner    The owner of this new task
    /// \param[in] executor The task's callback function
    /// \param[in] priority The priority of the task for scheduling
    ///                     by default it's NORMAL
    /// \param[in] delay    The time to execute after creating,
    ///                     by default it's 0
    /// \param[in] period   The period for a reapetitive task,
    ///                     by default it's 0
    ///
    /// \return A reference to the task's structure to act like
    ///         a handler
    ////////////////////////////////////////////////////////////
    virtual Task &syncTask(const PluginKey owner, TaskExecutor executor,
                           TaskPriority priority = TaskPriority::NORMAL, Tick delay = 0,
                           Tick period = 0) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Schedules an asynchronized task.
    ///
    /// The task will be executed by the thread manager.
    ///
    /// \param[in] owner    The owner of this new task
    /// \param[in] executor The task's callback function
    /// \param[in] priority The priority of the task for scheduling
    ///                     by default it's NORMAL
    /// \param[in] delay    The time to execute after creating,
    ///                     by default it's 0
    ///
    /// \return A reference to the task's structure to act like
    ///         a handler
    ////////////////////////////////////////////////////////////
    virtual Task &asyncTask(const PluginKey owner, TaskExecutor executor,
                            TaskPriority priority = TaskPriority::NORMAL, Tick delay = 0) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Removes all tasks of a plugin.
    ///
    /// \param[in] plugin The task's owner
    ////////////////////////////////////////////////////////////
    virtual void cancel(const PluginKey plugin) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Removes all tasks.
    ////////////////////////////////////////////////////////////
    virtual void cancelAll() = 0;
};

} // namespace Ghrum

#endif // _TASK_MANAGER_HPP_