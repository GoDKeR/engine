/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _SCHEDULER_HPP_
#define _SCHEDULER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "TaskManager.hpp"
#include "TaskWorker.hpp"
#include <vector>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define how many milliseconds are in a second.
////////////////////////////////////////////////////////////
#define MILLISECOND_TO_SECOND 1000

////////////////////////////////////////////////////////////
/// \brief Represent the main thread of the engine.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Scheduler : public TaskManager {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(Scheduler);

    ////////////////////////////////////////////////////////////
    /// \brief Returns all sheduler's workers.
    ///
    /// \return All scheduler's workers
    ////////////////////////////////////////////////////////////
    virtual std::vector<TaskWorker *const> getWorkers() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Stops the scheduler for running.
    ////////////////////////////////////////////////////////////
    virtual void stop() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Returns if scheduler is active.
    ///
    /// \return If scheduler is active
    ////////////////////////////////////////////////////////////
    virtual bool isActive() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Returns if the platform is under heavy load.
    ///
    /// The engine is considered under heavy load if the previous
    /// tick went over time, or if the current tick has gone over
    /// time.
    ///
    /// \return If the platform is under heavy load
    ////////////////////////////////////////////////////////////
    virtual bool isOverloaded() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the amount of time since the start of the tick.
    ///
    /// \return The amount of time since the start of the tick
    ////////////////////////////////////////////////////////////
    virtual Tick getUptime() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the amount of time remaining until the tick
    ///        should end.
    ///
    /// \return The time in milliseconds remaining
    ////////////////////////////////////////////////////////////
    virtual Tick getRemainingTickTime() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Returns the number of tick time per 1000ms.
    ///
    /// \return Number of tick time per 1000ms
    ////////////////////////////////////////////////////////////
    virtual uint16_t getIterationPerSecond() const = 0;
};

} // namespace Ghrum

#endif // _SCHEDULER_HPP_