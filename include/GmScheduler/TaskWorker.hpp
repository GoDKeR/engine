/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _TASK_WORKER_HPP_
#define _TASK_WORKER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Task.hpp"
#include <thread>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a task worker.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class TaskWorker {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Returns the current owner of the task.
    ///
    /// \return The current owner of the task
    ////////////////////////////////////////////////////////////
    virtual PluginKey getOwner() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Returns the current task owned by the worker.
    ///
    /// \return The current task owned by the worker
    ////////////////////////////////////////////////////////////
    virtual const Task &getTask() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Returns the thread's unique id.
    ///
    /// \return The thread's unique id
    ////////////////////////////////////////////////////////////
    virtual std::thread::id getThread() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Returns the thread's underlying handle.
    ///
    /// !WARNING! The handle is used for special case, where the
    /// developer needs access to OS functions.
    ///
    /// \return The thread's underlying handle
    virtual std::thread::native_handle_type getHandle() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Sets the thread's priority.
    ///
    /// \param[in] priority The thread's new priority
    ////////////////////////////////////////////////////////////
    virtual void setPriority(uint8_t priority) = 0;
};

} // namespace Ghrum

#endif // _TASK_WORKER_HPP_