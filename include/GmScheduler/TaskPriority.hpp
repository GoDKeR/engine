/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _TASK_PRIORITY_HPP_
#define _TASK_PRIORITY_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the task priority.
///
/// A higher priority task is likely to be executed over a low
/// priority task if the scheduler is overloaded.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
enum class TaskPriority : uint16_t {
    ////////////////////////////////////////////////////////////
    /// \brief Can be deferred by up to 10s when under load.
    ////////////////////////////////////////////////////////////
    LOWEST = 10000,

    ////////////////////////////////////////////////////////////
    /// \brief Can be deferred by up to 1.5s when under load.
    ////////////////////////////////////////////////////////////
    LOW = 1500,

    ////////////////////////////////////////////////////////////
    /// \brief Can be deferred by up to 500ms when under load.
    ////////////////////////////////////////////////////////////
    NORMAL = 500,

    ////////////////////////////////////////////////////////////
    /// \brief Can be deferred by up to 150ms when under load.
    ////////////////////////////////////////////////////////////
    HIGH = 150,

    ////////////////////////////////////////////////////////////
    /// \brief Can be deferred by up to 50ms when under load.
    ////////////////////////////////////////////////////////////
    HIGHEST = 50,

    ////////////////////////////////////////////////////////////
    /// \brief Will not deferred.
    ////////////////////////////////////////////////////////////
    CRITICAL = 0
};

} // namespace Ghrum

#endif // _TASK_PRIORITY_HPP_