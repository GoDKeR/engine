/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _TASK_HPP_
#define _TASK_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "TaskExecutor.hpp"
#include "TaskPriority.hpp"
#include <chrono>
#include <GmPlugin/Plugin.hpp>

////////////////////////////////////////////////////////////
/// Clock implementation
////////////////////////////////////////////////////////////
using Clock = std::chrono::steady_clock;

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represent a single task.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Task {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] owner      Owner of the task
    /// \param[in] executor   Function of the task
    /// \param[in] priority   The task's priority
    /// \param[in] tickTime   Next execution tick
    /// \param[in] periodTime The period of the execution
    /// \param[in] isParallel If the task is parallel
    ////////////////////////////////////////////////////////////
    Task(const PluginKey owner, TaskExecutor executor, TaskPriority priority,
         Tick tickTime, Tick periodTime, bool isParallel)
        : m_Owner(owner),
          m_Executor(executor),
          m_Priority(priority),
          m_TickTime(tickTime),
          m_PeriodTime(periodTime),
          m_Parallel(isParallel),
          m_Alive(true),
          m_Name("Undefined"),
          m_Time(Clock::now()) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Runs the task callback.
    ////////////////////////////////////////////////////////////
    inline void run() {
        m_Executor();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Attach a name into this task.
    ///
    /// \param[in] name The new name of this task
    ////////////////////////////////////////////////////////////
    inline void setName(const std::string &name) {
        m_Name = name;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Sets the next tick time of a repeating task.
    ///
    /// This function is used INTERNALLY.
    ///
    /// \param[in] tick The new tick time of this task
    ////////////////////////////////////////////////////////////
    inline void setTickTime(size_t tick) {
        m_TickTime = tick;;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Cancel this task.
    ////////////////////////////////////////////////////////////
    inline void setCancelled() {
        m_Alive = false;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the creation time of this task.
    ///
    /// \return The time when the task was created
    ////////////////////////////////////////////////////////////
    inline const Clock::time_point &getTime() const {
        return m_Time;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the name of this task.
    ///
    /// \return The name of this task
    ////////////////////////////////////////////////////////////
    inline const std::string &getName() const {
        return m_Name;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the owner of this task.
    ///
    /// \return The owner of this task
    ////////////////////////////////////////////////////////////
    inline const PluginKey getOwner() const {
        return m_Owner;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the priority of this task.
    ///
    /// \return The priority of this task
    ////////////////////////////////////////////////////////////
    inline TaskPriority getPriority() const {
        return m_Priority;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the period time of this task.
    ///
    /// \return The period time of this task
    ////////////////////////////////////////////////////////////
    inline Tick getPeriodTime() const {
        return m_PeriodTime;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the next time time of this task.
    ///
    /// \return The next time time of this task
    ////////////////////////////////////////////////////////////
    inline Tick getTickTime() const {
        return m_TickTime;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets if the task is alive.
    ///
    /// \return If the task is alive
    ////////////////////////////////////////////////////////////
    inline bool isAlive() const {
        return m_Alive;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets if the task executes parallel.
    ///
    /// \return If the task executes parallel
    ////////////////////////////////////////////////////////////
    inline bool isParallel() const {
        return m_Parallel;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets if the task repeat when finish.
    ///
    /// \return If the task repeat when finish
    ////////////////////////////////////////////////////////////
    inline bool isRepeating() const {
        return m_PeriodTime > 0;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const PluginKey    m_Owner;
    TaskExecutor       m_Executor;
    const TaskPriority m_Priority;
    Tick               m_TickTime, m_PeriodTime;
    const bool         m_Parallel;
    bool               m_Alive;
    std::string        m_Name;
    Clock::time_point  m_Time;
};

} // namespace Ghrum

#endif // _TASK_HPP_