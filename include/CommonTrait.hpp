/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ENGINE_TRAITS_HPP_
#define _ENGINE_TRAITS_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <type_traits>
#include <typeinfo>

////////////////////////////////////////////////////////////
/// \brief Enable the metafunction if class derives.
///
/// \tparam[in] Base      Base class.
/// \tparam[in] Derivated Derivated class.
////////////////////////////////////////////////////////////
#define enableIfBaseOf(Base, Derivated) \
    class = typename std::enable_if<std::is_base_of<Base, Derivated>::value>::type

////////////////////////////////////////////////////////////
/// \brief Enable the metafunction if param derives from
///        arithmetic.
///
/// \tparam[in] Type The param type.
////////////////////////////////////////////////////////////
#define enableIfArithmetic(Type) \
    typename = typename std::enable_if<std::is_arithmetic<Type>::value>::type

////////////////////////////////////////////////////////////
/// \brief Returns the hash_code implementation of a
///        expression.
///
/// \tparam[in] Base The param type.
////////////////////////////////////////////////////////////
#define getTemplateID(Base) \
    typeid(Base).hash_code()

////////////////////////////////////////////////////////////
/// \brief Returns the name implementation of a
///        expression.
///
/// \tparam[in] Base The param type.
////////////////////////////////////////////////////////////
#define getTemplateName(Base) \
    typeid(Base).name()

////////////////////////////////////////////////////////////
/// \brief Default constructor.
///
/// \tparam[in] Class The param type.
////////////////////////////////////////////////////////////
#define DEFAULT_CTOR(Class) \
    Class() = default;

////////////////////////////////////////////////////////////
/// \brief Default destructor.
///
/// \tparam[in] Class The param type.
////////////////////////////////////////////////////////////
#define DEFAULT_DTOR(Class) \
    virtual ~Class() noexcept = default;

////////////////////////////////////////////////////////////
/// \brief Default copy constructor.
///
/// \tparam[in] Class The param type.
////////////////////////////////////////////////////////////
#define DEFAULT_COPY_CTOR(Class) \
    Class(const Class &rhs) = default; \
    Class(Class && rhs) noexcept = default;

////////////////////////////////////////////////////////////
/// \brief Default copy constructor.
///
/// \tparam[in] Class The param type.
////////////////////////////////////////////////////////////
#define DEFAULT_COPY_CTOR_TEMPLATE(Class, Class2) \
    Class(const Class2 &rhs) = default; \
    Class(Class2 && rhs) noexcept = default;

////////////////////////////////////////////////////////////
/// \brief Default assigment operator
///
/// \tparam[in] Class The param type.
////////////////////////////////////////////////////////////
#define DEFAULT_ASSIGMENT(Class) \
    inline auto operator=(const Class &rhs) & -> Class & = default; \
    inline auto operator=(Class && rhs) &noexcept -> Class & = default;

////////////////////////////////////////////////////////////
/// \brief Disallow copy constructor.
///
/// \tparam[in] Class The param type.
////////////////////////////////////////////////////////////
#define DISALLOW_COPY_CTOR(Class) \
    Class(const Class &) = delete;

////////////////////////////////////////////////////////////
/// \brief Disallow assigment operator.
///
/// \tparam[in] Class The param type.
////////////////////////////////////////////////////////////
#define DISALLOW_ASSIGMENT(Class) \
    Class & operator=(const Class &) = delete;

#endif // _ENGINE_TRAITS_HPP_