/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMMON_HPP_
#define _COMMON_HPP_

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include "CommonTrait.hpp"
#include <string>
 
////////////////////////////////////////////////////////////
// Define the Ghrum version
////////////////////////////////////////////////////////////
#define GHRUM_VERSION_MAJOR 1
#define GHRUM_VERSION_MINOR 0

////////////////////////////////////////////////////////////
// Identify the operating system
////////////////////////////////////////////////////////////
#if defined(_WIN32) || defined(__WIN32__) || defined(_WIN64)

 	// Windows
 	#define GHRUM_SYSTEM_WINDOWS

#elif defined(linux) || defined(__linux) || defined(__gnu_linux__)

 	// Linux
 	#define GHRUM_SYSTEM_LINUX

#elif defined(__APPLE__) || defined(MACOSX) || defined(macintosh) || defined(Macintosh)

 	// MacOS
 	#define GHRUM_SYSTEM_MACOS

#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)

    // FreeBSD
    #define GHRUM_SYSTEM_FREEBSD

#else

    // Unsupported system
    #error This operating system is not supported by Ghrum library

#endif

////////////////////////////////////////////////////////////
// Define helpers to create portable import / export macros for each module
////////////////////////////////////////////////////////////
#if defined(GHRUM_SYSTEM_WINDOWS)

    // Windows compilers need specific (and different) keywords for export and import
    #define GHRUM_API_EXPORT __declspec(dllexport)
    #define GHRUM_API_IMPORT __declspec(dllimport)

#else // Linux, FreeBSD, Mac OS X

 	#if __GNUC__ >= 4

        // GCC 4 has special keywords for showing/hidding symbols,
        // the same keyword is used for both importing and exporting
        #define GHRUM_API_EXPORT __attribute__ ((__visibility__ ("default")))
        #define GHRUM_API_IMPORT __attribute__ ((__visibility__ ("default")))

 	#else

 		// GCC <4.0 is not supported.
 		#error This GCC build is not supported by Ghrum library

 	#endif

#endif

////////////////////////////////////////////////////////////
// Define common crossplatform stuff.
////////////////////////////////////////////////////////////
#ifdef GHRUM_SYSTEM_WINDOWS

    #define PATH_SEPARATOR "\\"

#else

    #define PATH_SEPARATOR "/"
 
#endif

namespace Ghrum {
    ////////////////////////////////////////////////////////////
    /// /brief The size of the ticks.
    ////////////////////////////////////////////////////////////
    typedef uint32_t Tick;
}

#endif // _COMMON_HPP_