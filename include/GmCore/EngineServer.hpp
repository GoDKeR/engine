/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ENGINE_SERVER_HPP_
#define _ENGINE_SERVER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Engine.hpp"
#include <GmProtocol/SessionManager.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the base class of the server engine.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class EngineServer : public Engine {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] name           The name of the implementation
    /// \param[in] version        The version of the implementation
    /// \param[in] pluginManager  The PluginManager implementation
    /// \param[in] eventManager   The EventManager implemenetation
    /// \param[in] scheduler      The scheduler implementation
    /// \param[in] assetManager   The asset manager implementation
    /// \param[in] sessionManager The session manager implementation
    ////////////////////////////////////////////////////////////
    EngineServer(const std::string &name,
                 const std::string &version,
                 std::unique_ptr<PluginManager> pluginManager,
                 std::unique_ptr<EventManager> eventManager,
                 std::unique_ptr<Scheduler> scheduler,
                 std::unique_ptr<AssetManager> assetManager,
                 std::unique_ptr<SessionManager> sessionManager)
        : Engine(Platform::Server,
                 name,
                 version,
                 std::move(pluginManager),
                 std::move(eventManager),
                 std::move(scheduler),
                 std::move(assetManager)),
        m_SessionManager(std::move(sessionManager)) {
    }
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::unique_ptr<SessionManager> m_SessionManager;
};

} // namespace Ghrum

#endif // _ENGINE_SERVER_HPP_