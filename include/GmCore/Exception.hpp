/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _EXCEPTION_HPP_
#define _EXCEPTION_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>
#include <exception>
#include <memory>

#ifdef GHRUM_SYSTEM_WINDOWS

    #include <windows.h>

#else

    #include <errno.h>
    #include <cstring>

#endif

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Interface of any engine's exception.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Exception : public std::exception {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] message The message of the exception
    ////////////////////////////////////////////////////////////
    Exception(const std::string & message)
        : m_Message(message),
          m_ChildException(nullptr) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] message The message of the exception
    /// \param[in] child   The child's message of the exception
    ////////////////////////////////////////////////////////////
    Exception(const std::string & message, std::shared_ptr<Exception> child)
        : m_Message(message),
          m_ChildException(std::move(child)) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    virtual ~Exception() throw() {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the error code of the system.
    ///
    /// \return An exception that carries the error code
    ////////////////////////////////////////////////////////////
    static std::shared_ptr<Exception> getSystemError() {
        std::shared_ptr<Exception> exception = nullptr;
#ifdef GHRUM_SYSTEM_WINDOWS
        LPTSTR wBuffer = nullptr;
        if (!::FormatMessage(
                    FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                    0x0000,
                    GetLastError(),
                    0x0000,
                    &wBuffer,
                    0x00,
                    NULL));
        {
            exception = std::make_shared<Exception>("Unknown system error.");
        } else {
            exception = std::make_shared<Exception>(wBuffer);
            ::LocalFree(wBuffer);
        }
#else
        exception = std::make_shared<Exception>(strerror(errno));
#endif
        return exception;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the exception's error message.
    ///
    /// \return The exception's error message
    ////////////////////////////////////////////////////////////
    inline const std::string & getMessage() const {
        return m_Message;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the full exception's error message.
    ///
    /// \return The full exception's error message
    ////////////////////////////////////////////////////////////
    inline const std::string getFullMessage() const {
        std::string rtnMessage(getMessage());
        if (m_ChildException != nullptr) {
            rtnMessage = '\n' + m_ChildException->getFullMessage();
        }
        return rtnMessage;
    }

    ////////////////////////////////////////////////////////////
    /// \copydoc std::exception::what()
    ////////////////////////////////////////////////////////////
    const char * what() const throw() {
        return m_Message.c_str();
    }
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const std::string m_Message;
    std::shared_ptr<Exception> m_ChildException;
};


} // namespace Ghrum

#endif // _EXCEPTION_HPP_