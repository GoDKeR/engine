/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ENGINE_HPP_
#define _ENGINE_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <GmEvent/EventManager.hpp>
#include <GmFilesystem/AssetManager.hpp>
#include <GmPlugin/PluginManager.hpp>
#include <GmScheduler/Scheduler.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the Engine plugin identifier.
////////////////////////////////////////////////////////////
#define ENGINE_PLUGIN_ID 0x0000

////////////////////////////////////////////////////////////
/// \brief Define the base class of the engine.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Engine {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] platform      The platform of the engine
    /// \param[in] name          The name of the implementation
    /// \param[in] version       The version of the implementation
    /// \param[in] pluginManager The PluginManager implementation
    /// \param[in] eventManager  The EventManager implemenetation
    /// \param[in] scheduler     The scheduler implementation
    /// \param[in] assetManager  The asset manager implementation
    ////////////////////////////////////////////////////////////
    Engine(Platform platform,
           const std::string &name,
           const std::string &version,
           std::unique_ptr<PluginManager> pluginManager,
           std::unique_ptr<EventManager> eventManager,
           std::unique_ptr<Scheduler> scheduler,
           std::unique_ptr<AssetManager> assetManager)
        : m_Name(name),
          m_Version(version),
          m_Platform(platform),
          m_PluginManager(std::move(pluginManager)),
          m_EventManager(std::move(eventManager)),
          m_Scheduler(std::move(scheduler)),
          m_AssetManager(std::move(assetManager)) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the name of the engine's implementation.
    ///
    /// \return The name of the engine's implementation
    ////////////////////////////////////////////////////////////
    inline const std::string &getName() const {
        return m_Name;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the version of the engine's implementation.
    ///
    /// \return The version of the engine's implementation
    ////////////////////////////////////////////////////////////
    inline const std::string &getVersion() const {
        return m_Version;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the current platform.
    ///
    /// \return The current platform
    ////////////////////////////////////////////////////////////
    inline Platform getPlatform() const {
        return m_Platform;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the plugin manager's implementation.
    ///
    /// \return The plugin manager's implementation
    ////////////////////////////////////////////////////////////
    inline PluginManager &getPluginManager() {
        return *m_PluginManager;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the event manager's implementation.
    ///
    /// \return The event manager's implementation
    ////////////////////////////////////////////////////////////
    inline EventManager &getEventManager() {
        return *m_EventManager;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the scheduler's implementation.
    ///
    /// \return The scheduler's implementation
    ////////////////////////////////////////////////////////////
    inline Scheduler &getScheduler() {
        return *m_Scheduler;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the asset manager implementation.
    ///
    /// \return The asset manager implementation
    ////////////////////////////////////////////////////////////
    inline AssetManager &getAssetManager() {
        return *m_AssetManager;
    }
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const Platform m_Platform;
    const std::string &m_Name;
    const std::string &m_Version;
    std::unique_ptr<PluginManager> m_PluginManager;
    std::unique_ptr<EventManager>  m_EventManager;
    std::unique_ptr<Scheduler>     m_Scheduler;
    std::unique_ptr<AssetManager>  m_AssetManager;
};

} // namespace Ghrum

#endif // _ENGINE_HPP_