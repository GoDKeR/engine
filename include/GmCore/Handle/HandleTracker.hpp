/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _HANDLE_TRACKER_HPP_
#define _HANDLE_TRACKER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Handle.hpp"
#include "HandleNode.hpp"
#include <set>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represents a node for the handles.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
template<typename T, enableIfArithmetic(T)>
class HandleTracker {
public:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef HandleNode<T> Type;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Disallow Copy constructor and assigment.
    ////////////////////////////////////////////////////////////
    DISALLOW_COPY_CTOR(HandleTracker<T>);
    DISALLOW_ASSIGMENT(HandleTracker<T>);

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ////////////////////////////////////////////////////////////
    HandleTracker() : HandleTracker(Type::MAX_VALUE) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Custom constructor.
    ///
    /// \param[in] max The max value of the tracker.
    ////////////////////////////////////////////////////////////
    HandleTracker(T max) {
        m_Array.insert(Type(1, max));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Allocate a new handle in the tracker.
    ///
    /// \return The new handle allocated.
    ////////////////////////////////////////////////////////////
    const Handle<T> getHandle() {
        auto iterator = m_Array.begin();
        auto pFirst = *(iterator);
        auto index  = pFirst.getLeft();

        m_Array.erase(iterator);
        if (index + 1 <= pFirst.getRight()) {
            m_Array.insert(Type(index + 1, pFirst.getRight()));
        }
        return Handle<T>(index);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Release a handle from the tracker.
    ///
    /// \param[in] handle The handler to invalidate.
    ////////////////////////////////////////////////////////////
    void releaseHandle(const Handle<T> &handle) {
        auto value    = handle.getValue();
        auto node     = Type(value, value);
        auto iterator = m_Array.find(node);

        if (iterator != m_Array.end() && iterator->getLeft() <= value
                && iterator->getRight() > value) {
            return;
        }
        iterator = m_Array.upper_bound(node);
        if (iterator == m_Array.end()) {
            return;
        }

        auto pFree   = *(iterator);
        if (value + 1 == pFree.getLeft()) {
            if (iterator != m_Array.begin()) {
                auto nIterator = iterator--;
                if (nIterator->getRight() + 1 == value) {
                    auto pNextFree = *(nIterator);

                    m_Array.erase(iterator);
                    m_Array.erase(nIterator);
                    m_Array.insert(Type(pNextFree.getLeft(), pFree.getRight()));
                } else {
                    m_Array.erase(iterator);
                    m_Array.insert(Type(value, pFree.getRight()));
                }
            } else {
                m_Array.erase(iterator);
                m_Array.insert(Type(value, pFree.getRight()));
            }
        } else
            m_Array.insert(Type(value, value));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Mark a handle to be pre-allocated in the tracker.
    ///
    /// \param[in] id The id of the handle.
    ///
    /// \return The new handle allocated.
    ////////////////////////////////////////////////////////////
    const Handle<T> setHandle(const T value) {
        auto iterator = m_Array.find(Type(value, value));
        if (iterator != nullptr) {
            auto pFirst = (*iterator);

            m_Array.erase(iterator);
            if (pFirst.getLeft() < value) {
                m_Array.insert(Type(pFirst.getLeft(), value - 1));
            } else if (pFirst.getRight() > value + 1) {
                m_Array.insert(Type(value + 1, pFirst.getRight()));
            }
            return true;
        }
        return false;
    }
private:
    ////////////////////////////////////////////////////////////
    /// Member data
    ////////////////////////////////////////////////////////////
    std::set<Type> m_Array; ///< Contains every node available.
};

} // namespace Ghrum

#endif // _HANDLE_TRACKER_HPP_