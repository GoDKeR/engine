/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _HANDLE_NODE_HPP_
#define _HANDLE_NODE_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>
#include <limits>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represents a node for the handles.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
template<typename T, enableIfArithmetic(T)>
class HandleNode {
public:
    ////////////////////////////////////////////////////////////
    /// Static member data
    ////////////////////////////////////////////////////////////
    static const T MAX_VALUE = std::numeric_limits<T>::max();
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] min The left value of the node.
    /// \param[in] max The right value of the node.
    ////////////////////////////////////////////////////////////
    HandleNode(T min, T max)
        : m_Min(min),
          m_Max(max) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Override < operator.
    ///
    /// \param[in] other The other node to compare.
    ///
    /// \return If this node is less than the given node.
    ////////////////////////////////////////////////////////////
    inline bool operator<(const HandleNode<T> &other) const {
        return (m_Min < other.m_Min) && (m_Max < other.m_Max);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Return the left value of the node.
    ///
    /// \return The left value of the node.
    ////////////////////////////////////////////////////////////
    inline T getLeft() const {
        return m_Min;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Return the right value of the node.
    ///
    /// \return The right value of the node.
    ////////////////////////////////////////////////////////////
    inline T getRight() const {
        return m_Max;
    }
private:
    ////////////////////////////////////////////////////////////
    /// Member data
    ////////////////////////////////////////////////////////////
    const T m_Min, m_Max; ///< Min and max value of the node.
};

} // namespace Ghrum

#endif // _HANDLE_NODE_HPP_