/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ADDRESS_ENDPOINT_HPP_
#define _ADDRESS_ENDPOINT_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Address.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a structure to hold internet address.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class AddressEndpoint {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] address The address bounded
    /// \param[in] port    The port bounded
    ////////////////////////////////////////////////////////////
    AddressEndpoint(const Address &address, uint16_t port)
        : m_Address(address),
          m_Port(port) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the address.
    ///
    /// \return The address
    ////////////////////////////////////////////////////////////
    inline const Address &getAddress() const {
        return m_Address;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the port bounded.
    ///
    /// \return The port bounded
    ////////////////////////////////////////////////////////////
    inline uint16_t getPort() const {
        return m_Port;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const Address  m_Address;
    const uint16_t m_Port;
};

} // namespace Ghrum

#endif // _ADDRESS_ENDPOINT_HPP_