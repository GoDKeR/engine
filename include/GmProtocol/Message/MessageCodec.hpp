/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _MESSAGE_CODEC_HPP_
#define _MESSAGE_CODEC_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "MessageHandler.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represent the model of a message.
///
/// The codec contains the view and controller of the data.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class MessageCodec : public MessageHandler {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Returns the size of the message.
    ///
    /// If the message's size is unknown at compile-time then
    /// returns 0xFFFFFFFF.
    ///
    /// \return The size of the message
    ////////////////////////////////////////////////////////////
    virtual uint32_t getSize() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Builds the message from the codec.
    ///
    /// \return The message of the codec
    ////////////////////////////////////////////////////////////
    virtual std::shared_ptr<Message> getMessage() const = 0;
};

} // namespace Ghrum

#endif // _MESSAGE_CODEC_HPP_