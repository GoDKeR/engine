/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _MESSAGE_HANDLER_HPP_
#define _MESSAGE_HANDLER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Message.hpp"
#include <GmProtocol/Session.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represent the handler of a message.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class MessageHandler {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Handles the message.
    ///
    /// \param[in] session The reciever of the message
    /// \param[in] message The message to handle
    ////////////////////////////////////////////////////////////
    virtual void handle(std::shared_ptr<Session> session,
                        std::shared_ptr<Message> message) const = 0;
};

} // namespace Ghrum

#endif // _MESSAGE_HANDLER_HPP_