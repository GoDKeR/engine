/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _MESSAGE_HPP_
#define _MESSAGE_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "MessageInputStream.hpp"
#include "MessageOutputStream.hpp"
#include <memory>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// Typedef
////////////////////////////////////////////////////////////
typedef uint32_t MessageKey;

////////////////////////////////////////////////////////////
/// \brief Represent the data of a packet.
///
/// <ul>
/// <li>All message's fields should be immutable.</li>
/// <li>All fields in a Message should be primitive.</li>
/// </ul>
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Message {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Returns the unique id of the message.
    ///
    /// \return An unique id that represents the message
    ////////////////////////////////////////////////////////////
    virtual MessageKey getId() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads the message from a stream.
    ///
    /// \param[in] input The input-stream that hold the bytes.
    ////////////////////////////////////////////////////////////
    virtual void read(MessageInputStream &input) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes the message into a stream.
    ///
    /// \param[in] output The output-stream to store the bytes.
    ////////////////////////////////////////////////////////////
    virtual void write(MessageOutputStream &output) const = 0;
};

} // namespace Ghrum

#endif // _MESSAGE_HPP_