/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _MESSAGE_LOOKUP_SERVICE_HPP_
#define _MESSAGE_LOOKUP_SERVICE_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "MessageCodec.hpp"
#include <unordered_map>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a storage service for messages.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class MessageLookupService {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Register a new message into the lookup service.
    ///
    /// \tparam T Template parameter of type IMessageCodec
    /// \tparam J Template parameter of type Any
    ///
    /// \param[in] id         The message's id
    /// \param[in] parameters Constructor's arguments
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(MessageCodec, T), typename ...J>
    void registerMessage(MessageKey id, J... parameters) {
        m_Codec.emplace(id, std::unique_ptr<T>(new T(
                std::forward<J>(parameters)...)));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Retrieves the codec of the given message.
    ///
    /// \tparam T Template parameter of type IMessageCodec
    ///
    /// \param[in] id The message's id
    ///
    /// \return The message codec for the given message
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(MessageCodec, T)>
    T const *getMessageCodec(MessageKey id) const {
        const auto &pFind = m_Codec.find(id);
        return (pFind == std::end(m_Codec)
                ? nullptr
                : pFind->second.get());
    }
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::unordered_map<MessageKey, std::unique_ptr<MessageCodec>> m_Codec;
};

} // namespace Ghrum

#endif // _MESSAGE_LOOKUP_SERVICE_HPP_ 