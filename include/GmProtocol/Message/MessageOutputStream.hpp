/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _MESSAGE_OUTPUT_STREAM_HPP_
#define _MESSAGE_OUTPUT_STREAM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Generic class for custom message output streams.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class MessageOutputStream {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(MessageOutputStream);

    ////////////////////////////////////////////////////////////
    /// \brief Writes a number of bytes into a buffer.
    ///
    /// \param[inout] pData The buffer that hold the bytes
    /// \param[in]    size  The number of bytes to write
    ///
    /// \return The number of bytes written
    ////////////////////////////////////////////////////////////
    virtual size_t write(void *pData, size_t size) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the buffer's size.
    ///
    /// \return the buffer's size
    ////////////////////////////////////////////////////////////
    virtual size_t getLenght() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a boolean into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeBoolean(bool value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed byte into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeByte(int8_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned byte into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeUnsignedByte(uint8_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed short into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeShort(int16_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned short into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeUnsignedShort(uint16_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed integer into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeInteger(int32_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned integer into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeUnsignedInteger(uint32_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a signed long into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeLong(int64_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes an unsigned long into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeUnsignedLong(uint64_t value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a float into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeFloat(float value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a double into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeDouble(double value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a string encoded with UTF-8 into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeUTF8(const std::string &value) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes a string encoded with UTF-16 into the buffer.
    ///
    /// \param[in] value The value to writes
    ////////////////////////////////////////////////////////////
    virtual void writeUTF16(const std::u16string &value) = 0;
};

} // namespace Ghrum

#endif // _MESSAGE_OUTPUT_STREAM_HPP_