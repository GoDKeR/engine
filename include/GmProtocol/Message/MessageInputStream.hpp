/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _MESSAGE_INPUT_STREAM_HPP_
#define _MESSAGE_INPUT_STREAM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Generic class for custom message input streams.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class MessageInputStream {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(MessageInputStream);

    ////////////////////////////////////////////////////////////
    /// \brief Read a number of bytes into a buffer.
    ///
    /// \param[inout] pData The buffer to hold the bytes
    /// \param[in]    size  The number of bytes to read
    ///
    /// \return The number of bytes readed
    ////////////////////////////////////////////////////////////
    virtual size_t read(void *pData, size_t size) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Moves the position pointer.
    ///
    /// \param[in] position The new position
    ///
    /// \return The new position of the buffer
    ////////////////////////////////////////////////////////////
    virtual size_t seek(size_t position) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the current position.
    ///
    /// \return the current position
    ////////////////////////////////////////////////////////////
    virtual size_t getPosition() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the buffer's size.
    ///
    /// \return the buffer's size
    ////////////////////////////////////////////////////////////
    virtual size_t getLenght() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a boolean from the stream.
    ///
    /// \return A boolean from the stream
    ////////////////////////////////////////////////////////////
    virtual bool readBoolean() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a signed byte from the stream.
    ///
    /// \return A signed byte from the stream
    ////////////////////////////////////////////////////////////
    virtual int8_t readByte() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned byte from the stream.
    ///
    /// \return An unsigned byte from the stream
    ////////////////////////////////////////////////////////////
    virtual uint8_t readUnsignedByte() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a signed short from the stream.
    ///
    /// \return A signed short from the stream
    ////////////////////////////////////////////////////////////
    virtual int16_t readShort() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned short from the stream.
    ///
    /// \return An unsigned short from the stream
    ////////////////////////////////////////////////////////////
    virtual uint16_t readUnsignedShort() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a singned integer from the stream.
    ///
    /// \return A singned integer from the stream
    ////////////////////////////////////////////////////////////
    virtual int32_t readInteger() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned integer from the stream.
    ///
    /// \return An unsigned integer from the stream
    ////////////////////////////////////////////////////////////
    virtual uint32_t readUnsignedInteger() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a signed long from the stream.
    ///
    /// \return A signed long from the stream
    ////////////////////////////////////////////////////////////
    virtual int64_t readLong() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads an unsigned long from the stream.
    ///
    /// \return An unsigned long from the stream
    ////////////////////////////////////////////////////////////
    virtual uint64_t readUnsignedLong() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a float from the stream.
    ///
    /// \return A float from the stream
    ////////////////////////////////////////////////////////////
    virtual float readFloat() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a double from the stream.
    ///
    /// \return A double from the stream
    ////////////////////////////////////////////////////////////
    virtual double readDouble() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a string encoded with UTF-8 from the stream.
    ///
    /// \return A string encoded with UTF-8 from the stream
    ////////////////////////////////////////////////////////////
    virtual std::string readUTF8() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a string encoded with UTF-16 from the stream.
    ///
    /// \return A string encoded with UTF-16 from the stream
    ////////////////////////////////////////////////////////////
    virtual std::u16string readUTF16() = 0;
};

} // namespace Ghrum

#endif // _MESSAGE_INPUT_STREAM_HPP_