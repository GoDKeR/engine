/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _MESSAGE_FACTORY_HPP_
#define _MESSAGE_FACTORY_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Message.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Deploy common messages for a foundation.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class MessageFactory {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Gets the disconnect message the server sends to
    ///        clients on disconnected.
    ///
    /// \param[in] reason The reason why the session has been
    ///                   disconnected
    ///
    /// \return The message built
    ////////////////////////////////////////////////////////////
    virtual std::shared_ptr<Message> getDisconnectMessage(
        const std::string &reason) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the introduction message the client sends to
    ///        server on connect.
    ///
    /// \return The message built
    ////////////////////////////////////////////////////////////
    virtual std::shared_ptr<Message> getWelcomeMessage() const = 0;
};

} // namespace Ghrum

#endif // _MESSAGE_FACTORY_HPP_