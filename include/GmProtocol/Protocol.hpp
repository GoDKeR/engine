/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PROTOCOL_HPP_
#define _PROTOCOL_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Message/MessageFactory.hpp"
#include "ProtocolService.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a protocol for a game.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Protocol : public ProtocolService, public MessageFactory {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param name The name of the protocol
    /// \param port The default port of the protocol
    /// \param type The session's type
    ////////////////////////////////////////////////////////////
    Protocol(const std::string &name, uint16_t port, SessionType type)
        : m_Name(name),
          m_Port(port),
          m_SessionType(type) {
    }
    
    ////////////////////////////////////////////////////////////
    /// \brief Default virtual destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(Protocol);

    ////////////////////////////////////////////////////////////
    /// \brief Gets the name of the protocol.
    ///
    /// \return The name of the protocol
    ////////////////////////////////////////////////////////////
    inline const std::string &getName() const {
        return m_Name;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the default port of the protocol.
    ///
    /// \return The default port of the protocol
    ////////////////////////////////////////////////////////////
    inline uint16_t getPort() const {
        return m_Port;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the type of the sessions.
    ///
    /// \return The type of the sessions
    ////////////////////////////////////////////////////////////
    inline SessionType getType() const {
        return m_SessionType;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const std::string m_Name;
    const uint16_t    m_Port;
    const SessionType m_SessionType;
};

} // namespace Ghrum

#endif // _PROTOCOL_HPP_