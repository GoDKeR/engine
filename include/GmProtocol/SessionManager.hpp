/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _SESSION_MANAGER_HPP_
#define _SESSION_MANAGER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Protocol.hpp"
#include "Session.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the manager of every session.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class SessionManager {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Sets the protocol of the manager.
    ///
    /// \param[in] protocol The protocol
    ////////////////////////////////////////////////////////////
    virtual void setProtocol(const Protocol &protocol) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the protocol of the manager.
    ///
    /// \return The protocol of the manager
    ////////////////////////////////////////////////////////////
    virtual const Protocol &getProtocol() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Close the connection and every session attached.
    ///
    /// \param[in] reason The reason of the call
    ////////////////////////////////////////////////////////////
    virtual void close(const std::string &reason) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Disconnect every alive session.
    ///
    /// \param[in] reason The reason of the call
    ////////////////////////////////////////////////////////////
    virtual void disconnect(const std::string &reason) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Send a message to all alive sessions.
    ///
    /// \param[in] message The message to send
    ////////////////////////////////////////////////////////////
    virtual void sendMessageToAll(std::shared_ptr<Message> message) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Get a session.
    ///
    /// \param[in] id The unique id of the session
    ///
    /// \return The session given its id
    ////////////////////////////////////////////////////////////
    virtual Session *const getSession(SessionKey id) const = 0;
};

} // namespace Ghrum

#endif // _SESSION_MANAGER_HPP_