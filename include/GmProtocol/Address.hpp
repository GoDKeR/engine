/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ADDRESS_HPP_
#define _ADDRESS_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a structure to hold internet address.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Address {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ////////////////////////////////////////////////////////////
    Address() : m_Address(0) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Constructor given its integer representation.
    ///
    /// \param[in] address The address in form of integer
    ////////////////////////////////////////////////////////////
    Address(uint32_t address) : m_Address(address) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Constructor given its byte representation.
    ///
    /// \param[in] b0 The first byte of the address
    /// \param[in] b1 The second byte of the address
    /// \param[in] b2 The third byte of the address
    /// \param[in] b3 The last byte of the address
    ////////////////////////////////////////////////////////////
    Address(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3)
        : m_Address((b0 << 24) |
                    (b1 << 16) |
                    (b2 << 8) |
                    (b3)) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the address representation as integer.
    ///
    /// \return The address representation as integer
    ////////////////////////////////////////////////////////////
    inline uint32_t getAddress() const {
        return m_Address;
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    const uint32_t m_Address;
};

} // namespace Ghrum

#endif // _ADDRESS_HPP_