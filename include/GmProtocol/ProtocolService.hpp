/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PROTOCOL_SERVICE_HPP_
#define _PROTOCOL_SERVICE_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Message/MessageLookupService.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a protocol reader for the game.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ProtocolService : public MessageLookupService {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Provides the buffer for the sessions implementation.
    ///
    /// \param[in] output The output-stream of the session
    /// \param[in] input  The input-stream of the session
    ////////////////////////////////////////////////////////////
    virtual void provideBuffer(std::unique_ptr<MessageOutputStream> &output,
                               std::unique_ptr<MessageInputStream> &input) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Writes the message into an output stream using
    ///        the protocol.
    ///
    /// \param[in] output  The output-stream of the session
    /// \param[in] message The message to write
    ////////////////////////////////////////////////////////////
    virtual void writeMessage(MessageOutputStream &output,
                              std::shared_ptr<Message> message) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Reads a message from an input stream using the
    ///        protocol.
    ///
    /// \param[in] input The input-stream of the session
    ///
    /// \return The message parsed from the given stream
    ////////////////////////////////////////////////////////////
    virtual std::shared_ptr<Message> readMessage(
        MessageInputStream &input) const = 0;
};

} // namespace Ghrum

#endif // _PROTOCOL_SERVICE_HPP_