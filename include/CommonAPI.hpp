/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMMON_API_HPP_
#define _COMMON_API_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "GmCore/EngineServer.hpp"
#include "GmCore/EngineClient.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represent the singleton class to access the
///        entire engine from a plugin view perspective.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class EngineAPI {
private:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ////////////////////////////////////////////////////////////
    EngineAPI()
        : m_Engine(nullptr) {
    }
public:
    ////////////////////////////////////////////////////////////
    /// \brief Sets the instance of the singleton.
    ///
    /// \param[in] instance The instance of the engine.
    ////////////////////////////////////////////////////////////
    inline void setInstance(Engine &instance) {
        m_Engine = std::shared_ptr<Engine>(&instance);
    }
public:
    ////////////////////////////////////////////////////////////
    /// \brief Remove copy constructor and copy assigment.
    ////////////////////////////////////////////////////////////
    EngineAPI (const EngineAPI &) = delete;
    EngineAPI &operator=(const EngineAPI &) = delete;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the instance of the engine as server.
    ///
    /// \return The instance of the engine as server.
    ////////////////////////////////////////////////////////////
    static EngineServer &getServer() {
        return static_cast<EngineServer &>(*getInstance().m_Engine);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the instance of the engine as client.
    ///
    /// \return The instance of the engine as client.
    ////////////////////////////////////////////////////////////
    static EngineClient &getClient() {
        return static_cast<EngineClient &>(*getInstance().m_Engine);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the instance of the engine.
    ///
    /// \return The instance of the engine.
    ////////////////////////////////////////////////////////////
    static Engine &getEngine() {
        return static_cast<Engine &>(*getInstance().m_Engine);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the current platform of the engine.
    ///
    /// \return The current platform of the engine.
    ////////////////////////////////////////////////////////////
    static Platform getPlatform() {
        return getInstance().m_Engine->getPlatform();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the plugin manager of the engine.
    ///
    /// \return The plugin manager of the engine.
    ////////////////////////////////////////////////////////////
    static PluginManager &getPluginManager() {
        return getInstance().m_Engine->getPluginManager();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the event manager of the engine.
    ///
    /// \return The event manager of the engine.
    ////////////////////////////////////////////////////////////
    static EventManager &getEventManager() {
        return getInstance().m_Engine->getEventManager();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the scheduler of the engine.
    ///
    /// \return The scheduler of the engine.
    ////////////////////////////////////////////////////////////
    static Scheduler &getScheduler() {
        return getInstance().m_Engine->getScheduler();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the asset manager of the engine.
    ///
    /// \return The asset manager of the engine.
    ////////////////////////////////////////////////////////////
    static AssetManager &getAssetManager() {
        return getInstance().m_Engine->getAssetManager();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the singleton instance.
    ///
    /// The instance is created first time this function is
    /// called.
    ///
    /// \return The singleton instance.
    ////////////////////////////////////////////////////////////
    GHRUM_API_EXPORT static EngineAPI &getInstance() {
        static EngineAPI *s_Instance;
        if (!s_Instance) {
            s_Instance = new EngineAPI();
        }
        return *s_Instance;
    }
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::shared_ptr<Engine> m_Engine;
};

} // namespace Ghrum

#endif // _COMMON_API_HPP_