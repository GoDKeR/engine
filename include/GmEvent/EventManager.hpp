/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _EVENT_MANAGER_HPP_
#define _EVENT_MANAGER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "EventExecutor.hpp"
#include "EventPriority.hpp"
#include <GmPlugin/Plugin.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// Typedef
////////////////////////////////////////////////////////////
typedef Handle<uint16_t> EventKey;
typedef uint32_t         EventID;

////////////////////////////////////////////////////////////
/// \brief Manages event's delegation.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class EventManager {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(EventManager);

    ////////////////////////////////////////////////////////////
    /// \brief Construct and deferred an event.
    ///
    /// \tparam T Template parameter T of type Event
    /// \tparam J Template parameter J of type Any
    ///
    /// \param[in] parameters Event's parameters
    ///
    /// \return The event constructed
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Event, T), typename ...J>
    inline T postEvent(J... parameters) const {
        T pEvent(std::forward<J>(parameters)...);
        postEvent(pEvent, getTemplateID(T));
        return pEvent;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Construct and deferred an asyncronized event.
    ///
    /// \tparam T Template parameter T of type Event
    /// \tparam J Template parameter J of type Any
    ///
    /// \param[in] parameters Event's parameters
    ///
    /// \return The event constructed
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Event, T), typename ...J>
    inline T postEventAsync(J... parameters) const {
        auto pEvent(std::make_shared<T>(std::forward(parameters)...));
        postEventAsync(pEvent, getTemplateID(T));
        return pEvent;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Construct and deferred an asyncronized event.
    ///
    /// After propagation ends, call a completation handler.
    ///
    /// \tparam T Template parameter T of type Event
    /// \tparam J Template parameter J of type Any
    ///
    /// \param[in] parameters Event's parameters
    ///
    /// \return The event constructed
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Event, T), typename ...J>
    inline T postEventAsync(EventExecutor executor, J... parameters) const {
        auto pEvent(std::make_shared<T>(std::forward(parameters)...));
        postEventAsync(pEvent, executor, getTemplateID(T));
        return pEvent;
    };

    ////////////////////////////////////////////////////////////
    /// \brief Register an event's callback.
    ///
    /// \tparam T Template parameter of type Event
    /// \tparam J Template parameter of type Function
    ///
    /// \param[in] owner    The owner of the event
    /// \param[in] executor The event's function
    /// \param[in] priority The function's priority
    ///
    /// \return An unique identifier to be able to unregister
    ///         the event
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Event, T)>
    inline EventKey registerEvent(const PluginKey &owner, EventExecutor callback,
                                  EventPriority priority = NORMAL) {
        return registerEvent(owner, callback, priority, getTemplateID(T));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Unregister an event's callback.
    ///
    /// \tparam T Template parameter T of type Event
    ///
    /// \param[in] owner The owner of the event
    /// \param[in] id    The id given on registration
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Event, T)>
    inline void unregisterEvent(const PluginKey &owner, EventKey id) {
        unregisterEvent(owner, getTemplateID(T), id);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Removes every event registered by an owner.
    ///
    /// \param[in] owner The owner used to register events
    ////////////////////////////////////////////////////////////
    virtual void remove(const PluginKey &owner) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Removes all event registered.
    ////////////////////////////////////////////////////////////
    virtual void removeAll() = 0;
private:
    ////////////////////////////////////////////////////////////
    /// \brief Calls an event syncronized.
    ///
    /// \param[in] event The event to propagate.
    /// \param[in] id    The event's id.
    ////////////////////////////////////////////////////////////
    virtual void postEvent(Event &event, EventID id) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Calls an event asyncronized.
    ///
    /// The event will be called on another thread, rather than
    /// the main thread.
    ///
    /// \param[in] event The event to propagate.
    /// \param[in] id    The event's id.
    ////////////////////////////////////////////////////////////
    virtual void postEventAsync(std::shared_ptr<Event> event, EventID id) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Calls an event asyncronized.
    ///
    /// The event will be called on another thread, rather than
    /// the main thread. After the propagation ends, call a
    /// completation handler.
    ///
    /// \param[in] event    The event to propagate.
    /// \param[in] executor The completation handler.
    /// \param[in] id       The event's id.
    ////////////////////////////////////////////////////////////
    virtual void postEventAsync(std::shared_ptr<Event> event,
                                EventExecutor executor, EventID id) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Register an event.
    ///
    /// @see unregisterEvent(owner, id, functionId)
    ///
    /// \param[in] owner    The owner of the event.
    /// \param[in] executor The event's function.
    /// \param[in] priority The function's priority.
    /// \param[in] id       The event's id.
    ///
    /// \return An unique identifier to be able to unregister
    ///         the event.
    ////////////////////////////////////////////////////////////
    virtual EventKey registerEvent(const PluginKey &owner, EventExecutor executor,
                                   EventPriority priority, EventID id) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Unregistered an event.
    ///
    /// @see registerEvent(owner, executor, priority, id)
    ///
    /// \param[in] owner The owner of the event.
    /// \param[in] id    The event's id.
    /// \param[in] key   The id given on registration.
    ////////////////////////////////////////////////////////////
    virtual void unregisterEvent(const PluginKey &owner, EventID id,
                                 EventKey key) = 0;
};

} // namespace Ghrum

#endif // _EVENT_MANAGER_HPP_