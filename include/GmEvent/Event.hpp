/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _EVENT_HPP_
#define _EVENT_HPP_

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represents an event.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Event {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ////////////////////////////////////////////////////////////
    Event()
        : m_Cancelled(false) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Return if event was cancelled.
    ///
    /// Returning TRUE will prevent event's propagation.
    ///
    /// \return If the event was cancelled
    ////////////////////////////////////////////////////////////
    inline bool isCancelled() const {
        return false;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Sets the cancellation state of the event.
    ///
    /// A cancelled event will not be executed in the platform,
    /// but will still pass to other plugins.
    ///
    /// \param[in] cancelled Cancellation flag
    ////////////////////////////////////////////////////////////
    inline void setCancelled(bool cancelled) {
        this->m_Cancelled = cancelled;
    }
protected:
    ////////////////////////////////////////////////////////////
    /// Member data
    ////////////////////////////////////////////////////////////
    bool m_Cancelled;
};

} // namespace Ghrum

#endif // _EVENT_HPP_