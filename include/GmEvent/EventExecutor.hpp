/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _EVENT_EXECUTOR_HPP_
#define _EVENT_EXECUTOR_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Event.hpp"
#include <functional>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a way to get function callback easily.
///
/// \param[in] Function The function
////////////////////////////////////////////////////////////
#define getFunctionEvent(Function) \
    EventExecutor(Function)

////////////////////////////////////////////////////////////
/// \brief Define a way to get member callback easily.
///
/// \param[in] Type The type of the member
/// \param[in] Instance The Type's instance
////////////////////////////////////////////////////////////
#define getMemberEvent(Type, Instance) \
    EventExecutor(std::bind(&Type, Instance, std::placeholders::_1))

////////////////////////////////////////////////////////////
/// \brief Represents an event's executor.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class EventExecutor {
public:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef std::function<void (Event &)> Generic;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \tparam T Template of type Function.
    ///
    /// \param[in] executor The function executor
    ////////////////////////////////////////////////////////////
    template<typename T>
    EventExecutor(T &&executor)
        : m_Function(reinterpret_cast<Generic &>(executor)) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \tparam T Template of type Function.
    ///
    /// \param[in] executor The function executor
    ////////////////////////////////////////////////////////////
    template<typename T>
    EventExecutor(const T &&executor)
        : m_Function(reinterpret_cast<Generic &>(executor)) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Call operator.
    ///
    /// \param[in] event The event deferred
    ////////////////////////////////////////////////////////////
    void operator()(Event &event) {
        m_Function(std::forward<Event &>(event));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Call operator.
    ///
    /// \param[in] event The event deferred
    ////////////////////////////////////////////////////////////
    void operator()(Event &event) const {
        m_Function(std::forward<Event &>(event));
    }
protected:
    ////////////////////////////////////////////////////////////
    /// Member data
    ////////////////////////////////////////////////////////////
    Generic m_Function;
};

} // namespace Ghrum

#endif // _EVENT_EXECUTOR_HPP_