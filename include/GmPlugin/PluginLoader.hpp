/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PLUGIN_LOADER_HPP_
#define _PLUGIN_LOADER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Plugin.hpp"
#include <memory>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represent a loader for plugins.
///
/// Each plugin type needs to register a plugin loader to be
/// able to identify it.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class PluginLoader {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(PluginLoader);

    ////////////////////////////////////////////////////////////
    /// \brief Gets the extension of the plugin.
    ///
    /// \return The extension of the plugin
    ////////////////////////////////////////////////////////////
    virtual const std::string getType() const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Loads a plugin given its descriptor.
    ///
    /// \param[in] id         The plugin's id
    /// \param[in] descriptor The plugin's descriptor
    ///
    /// \return The plugin structure populated
    ////////////////////////////////////////////////////////////
    virtual std::shared_ptr<Plugin> loadPlugin(PluginKey id,
            PluginDescriptor &descriptor) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Notify the loader that a plugin has been unloaded.
    ///
    /// \param[in] plugin The plugin notified
    ////////////////////////////////////////////////////////////
    virtual void onPluginUnload(Plugin &plugin) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Notify the loader that a plugin has been loaded.
    ///
    /// \param[in] plugin The plugin notified
    ////////////////////////////////////////////////////////////
    virtual void onPluginLoad(Plugin &plugin) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Notify the loader that a plugin has been disabled.
    ///
    /// \param[in] plugin The plugin notified
    ////////////////////////////////////////////////////////////
    virtual void onPluginDisable(Plugin &plugin) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Notify the loader that a plugin has been enabled.
    ///
    /// \param[in] plugin The plugin notified
    ////////////////////////////////////////////////////////////
    virtual void onPluginEnable(Plugin &plugin) const = 0;
};

} // namespace Ghrum

#endif // _PLUGIN_LOADER_HPP_