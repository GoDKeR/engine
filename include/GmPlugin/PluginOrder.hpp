/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PLUGIN_ORDER_HPP_
#define _PLUGIN_ORDER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Enumeration that holds a plugin initilization
///        order.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
enum class PluginOrder : uint8_t {
    ////////////////////////////////////////////////////////////
    /// \brief The plugin is loaded before the world generation.
    ////////////////////////////////////////////////////////////
    OnInitialize,

    ////////////////////////////////////////////////////////////
    /// \brief The plugin is loaded after the world generation.
    ////////////////////////////////////////////////////////////
    OnWorld
};

////////////////////////////////////////////////////////////
/// \brief Gets the enumeration value given its name.
///
/// \param[in] order The order's string representation.
///
/// \return
////////////////////////////////////////////////////////////
inline PluginOrder getPluginOrder(const std::string &order) {
    if (order == "OnInitialize") return PluginOrder::OnInitialize;
    else                         return PluginOrder::OnWorld;
}

} // namespace Ghrum

#endif // _PLUGIN_ORDER_HPP_