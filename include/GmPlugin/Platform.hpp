/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PLATFORM_HPP_
#define _PLATFORM_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Common.hpp>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Enumeration that describes whether the plugin was
///        written for client, server, or both.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
enum class Platform : uint8_t {
    ////////////////////////////////////////////////////////////
    /// \brief Client mode only.
    ////////////////////////////////////////////////////////////
    Client,

    ////////////////////////////////////////////////////////////
    /// \brief Server mode only.
    ////////////////////////////////////////////////////////////
    Server,

    ////////////////////////////////////////////////////////////
    /// \brief Both mode.
    ////////////////////////////////////////////////////////////
    Both
};

////////////////////////////////////////////////////////////
/// \brief Gets the enumeration value given its name.
///
/// \param[in] platform The platform's string representation.
///
/// \return
////////////////////////////////////////////////////////////
inline Platform getPlatform(const std::string &platform) {
    if (platform == "Client") return Platform::Client;
    if (platform == "Server") return Platform::Server;
    else                      return Platform::Both;
}

} // namespace Ghrum

#endif // _PLATFORM_HPP_