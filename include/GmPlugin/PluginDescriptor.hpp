/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PLUGIN_DESCRIPTOR_HPP_
#define _PLUGIN_DESCRIPTOR_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Platform.hpp"
#include "PluginOrder.hpp"
#include <vector>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define default values when loading the descriptor.
////////////////////////////////////////////////////////////
#define DEFAULT_NAME     "Unknown"
#define DEFAULT_WEBSITE  "Unknown"
#define DEFAULT_PLATFORM "Both"
#define DEFAULT_ORDER    "OnWorld"
#define DEFAULT_TYPE     "Assembly"
#define DEFAULT_VERSION  -1
#define DEFAULT_FILENAME "descriptor.yml"

////////////////////////////////////////////////////////////
/// \brief Holds every plugin's information.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
typedef struct PluginDescriptor {
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::string Name;                      ///< Plugin's name.
    std::string Website;                   ///< Plugin's website.
    std::string Folder;                    ///< Plugin's folder.
    std::string Type;                      ///< Plugin's type.
    std::vector<std::string> Authors;      ///< Plugin's authors.
    std::vector<std::string> Dependencies; ///< Dependencies.
    Platform Platform;                     ///< Plugin's platform.
    PluginOrder Order;                     ///< Plugin's order.
    uint32_t Version;                      ///< Plugin's version.
    std::vector<std::string> SoftDependencies; ///< Soft dependencies.

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] folder The folder of the descriptor
    ////////////////////////////////////////////////////////////
    PluginDescriptor(const std::string &folder)
        : Folder(folder) {
    }
} PluginDescriptor;

} // namespace Ghrum

#endif // _PLUGIN_DESCRIPTOR_HPP_