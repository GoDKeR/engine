/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PLUGIN_HPP_
#define _PLUGIN_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "PluginDescriptor.hpp"
#include <GmCore/Handle/Handle.hpp>
#include <memory>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the type id of a plugin.
////////////////////////////////////////////////////////////
typedef Handle<uint32_t> PluginKey;

////////////////////////////////////////////////////////////
/// \brief Represent a plugin.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class Plugin {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Disallow copy constructor and assigment.
    ////////////////////////////////////////////////////////////
    DISALLOW_COPY_CTOR(Plugin);
    DISALLOW_ASSIGMENT(Plugin);

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] id         The plugin's id
    /// \param[in] descriptor The descriptor of the plugin
    ////////////////////////////////////////////////////////////
    Plugin(PluginKey id, PluginDescriptor &descriptor)
        : m_Enabled(false),
          m_ID(id),
          m_Descriptor(descriptor) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(Plugin);

    ////////////////////////////////////////////////////////////
    /// \brief Gets the plugin's descriptor.
    ///
    /// \return The plugin's descriptor
    ////////////////////////////////////////////////////////////
    inline const PluginDescriptor &getDescriptor() const {
        return m_Descriptor;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the plugin's current folder.
    ///
    /// \return The plugin's current folder
    ////////////////////////////////////////////////////////////
    inline const std::string &getFolder() const {
        return m_Descriptor.Folder;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the name of the plugin.
    ///
    /// \return The name of the plugin
    ////////////////////////////////////////////////////////////
    inline const std::string &getName() const {
        return m_Descriptor.Name;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns if the plugin is enabled.
    ///
    /// \return If the plugin is enabled
    ////////////////////////////////////////////////////////////
    inline bool isEnabled() const {
        return m_Enabled;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Sets the current plugin's enabled state.
    ///
    /// \param[in] enabled The plugin's new state
    ////////////////////////////////////////////////////////////
    inline void setEnabled(bool enabled) {
        m_Enabled = enabled;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the unique id of the plugin.
    ///
    /// \return The unique id of the plugin
    ////////////////////////////////////////////////////////////
    inline PluginKey getId() const {
        return m_ID;
    }
protected:
    ////////////////////////////////////////////////////////////
    /// Member data
    ////////////////////////////////////////////////////////////
    bool m_Enabled;
    PluginKey m_ID;
    PluginDescriptor m_Descriptor;
};

} // namespace Ghrum

#endif // _PLUGIN_HPP_