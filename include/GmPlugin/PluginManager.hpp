/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _PLUGIN_MANAGER_HPP_
#define _PLUGIN_MANAGER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "PluginLoader.hpp"
#include <unordered_map>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Represent the manager of all plugins.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class PluginManager {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(PluginManager);

    ////////////////////////////////////////////////////////////
    /// \brief Register a plugin loader.
    ///
    /// \param[in] loader The loader to register
    ////////////////////////////////////////////////////////////
    virtual void registerLoader(std::unique_ptr<PluginLoader> loader) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets the plugin loader.
    ///
    /// \param[in] type The type of the loader
    ///
    /// \return The plugin's loader with the given type
    ////////////////////////////////////////////////////////////
    virtual PluginLoader *const getLoader(const std::string &type) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Loads every plugin in the folder that wasn't loaded.
    ////////////////////////////////////////////////////////////
    virtual void loadAll() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Check if the plugin is on memory.
    ///
    /// Please note that the name of the plugin is case-sensitive.
    ///
    /// \param[in] name The name of the plugin
    ///
    /// \return True if the plugin is on memory, otherwise False
    ////////////////////////////////////////////////////////////
    virtual bool isLoaded(const std::string &name) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Loads a plugin into memory.
    ///
    /// \param[in] name The name of the plugin to load
    ///
    /// \return True if function succeed, otherwise False
    ////////////////////////////////////////////////////////////
    virtual bool loadPlugin(const std::string &name) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Unloads a plugin from memory.
    ///
    /// \param[in] plugin The plugin
    ////////////////////////////////////////////////////////////
    virtual void unloadPlugin(Plugin &plugin) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Unloads every plugin that resides in memory.
    ////////////////////////////////////////////////////////////
    virtual void unloadAll() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Enable the plugin provided.
    ///
    /// <p>
    /// Attempting to enable a plugin that is already enabled
    /// will have no effect.
    ///
    /// \param[in] plugin The plugin
    ////////////////////////////////////////////////////////////
    virtual void enablePlugin(Plugin &plugin) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Enable every plugin that was loaded but never
    ///        enabled.
    ///
    /// The plugin must match the order provided.
    ///
    /// \param[in] order The plugin's order
    ////////////////////////////////////////////////////////////
    virtual void enableAll(PluginOrder order) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Disable the plugin provided.
    ///
    /// <p>
    /// Attempting to disable a plugin that is already enabled
    /// will have no effect.
    ///
    /// \param[in] plugin The plugin
    ////////////////////////////////////////////////////////////
    virtual void disablePlugin(Plugin &plugin) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Disable every plugin that resides in memory.
    ////////////////////////////////////////////////////////////
    virtual void disableAll() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Checks if the given plugin is loaded and returns
    ///        it when applicable
    ///
    /// Please note that the name of the plugin is case-sensitive/
    ///
    /// \param name The name of the plugin
    ///
    /// \return A pointer to the plugin if exist, otherwise null
    ////////////////////////////////////////////////////////////
    virtual Plugin *getPlugin(const std::string &name) const = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Get a list of all currently loaded plugins.
    ///
    /// \return A list of loaded plugins
    ////////////////////////////////////////////////////////////
    virtual std::vector<Plugin *> getPlugins() const = 0;
};

} // namespace Ghrum

#endif // _PLUGIN_MANAGER_HPP_