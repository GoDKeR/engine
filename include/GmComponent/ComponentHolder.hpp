/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMPONENT_HOLDER_HPP_
#define _COMPONENT_HOLDER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "ComponentManager.hpp"
#include <GmCore/Handle/Handle.hpp>
#include <unordered_map>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a entity to be just an ID for the component
///        holder, wrapped in a handle.
////////////////////////////////////////////////////////////
typedef Handle<uint32_t> EntityKey;

////////////////////////////////////////////////////////////
/// \brief Define the base class for any holder.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ComponentHolder {
private:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef std::unordered_map<ComponentKey, Component *> ComponentPointers;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ///
    /// \param[in] id    The unique id of the entity
    /// \param[in] owner The owner of this holder
    ////////////////////////////////////////////////////////////
    ComponentHolder(EntityKey id, ComponentManager &owner)
        : m_ID(id),
          m_Active(true),
          m_Owner(owner) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Return if the entity is active.
    ///
    /// \return True if the entity is active, otherwise False
    ////////////////////////////////////////////////////////////
    inline bool isActive() const {
        return m_Active;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Set the active flag of the entity.
    ///
    /// \param[in] value The new active flag
    ////////////////////////////////////////////////////////////
    inline void setActive(bool value) {
        if (value != m_Active) {
            setComponentsActive(value);
        }
        m_Active = value;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Removes a component from the entity.
    ///
    /// \tparam T Template of type Component
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T)>
    inline void removeComponent() {
        removeComponent(T::TYPE_ID);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Removes a component from the entity.
    ///
    /// \param[in] id The entity's id
    ////////////////////////////////////////////////////////////
    inline void removeComponent(ComponentKey id) {
        const auto &iterator = m_Components.find(id);
        if (iterator != std::end(m_Components))
            eraseComponent(iterator);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets a component from the holder.
    ///
    /// \tparam T Template of type Component
    ///
    /// \return The component retrieved
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T)>
    inline T *getComponent() const {
        return reinterpret_cast<T *>(getComponent(T::TYPE_ID));
    }

    ////////////////////////////////////////////////////////////
    /// \brief Retrieve a component from the entity's component
    ///        bag.
    ///
    /// \param[in] id The id of the component
    ///
    /// \return The component retrieved
    ////////////////////////////////////////////////////////////
    inline Component *getComponent(ComponentKey id) const {
        const auto &iterator = m_Components.find(id);
        return (iterator == std::end(m_Components)
                ? nullptr
                : iterator->second);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Check if the component is valid within the entity.
    ///
    /// \tparam T Template of type Component
    ///
    /// \return True if the component is contained
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T)>
    inline bool hasComponent() const {
        return hasComponent(T::TYPE_ID);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Check if the component is valid within the entity.
    ///
    /// \param[in] id The id of the component
    ///
    /// \return True if the component is contained
    ////////////////////////////////////////////////////////////
    inline bool hasComponent(ComponentKey id) const {
        return m_Components.find(id) != std::end(m_Components);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Invalidates all components within the entity.
    ////////////////////////////////////////////////////////////
    inline void invalidate() {
        for (const auto & iterator : m_Components) {
            iterator.second->setInvalid();
        }
        m_Components.clear();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Adds a component into the holder.
    ///
    /// \tparam T Template of type Component
    /// \tparam J Template of type Any
    ///
    /// \param[in] parameter Component's parameters
    ///
    /// \return The component object
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T), typename ...J>
    inline T &addComponent(J &&... parameter) {
        return m_Owner.addComponent<T>(std::forward<J...>(parameter)...);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the unique id of the entity.
    ///
    /// \return The unique id of the entity
    ////////////////////////////////////////////////////////////
    inline EntityKey getKey() const {
        return m_ID;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the begining of the component iterator.
    ///
    /// \return The begining of the component iterator.
    ////////////////////////////////////////////////////////////
    inline ComponentPointers::const_iterator begin() const {
        return std::begin(m_Components);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Returns the end of the component iterator.
    ///
    /// \return The end of the component iterator.
    ////////////////////////////////////////////////////////////
    inline ComponentPointers::const_iterator end() const {
        return std::end(m_Components);
    }
private:
    ////////////////////////////////////////////////////////////
    /// \brief Erase a component from this entity.
    ///
    /// \param[in] iterator The iterator to erase
    ////////////////////////////////////////////////////////////
    inline void eraseComponent(const ComponentPointers::iterator &iterator) {
        iterator->second->setInvalid();
        m_Components.erase(iterator);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Set the active flag of the components.
    ///
    /// \param[in] value The new active flag
    ////////////////////////////////////////////////////////////
    inline void setComponentsActive(bool value) {
        for (const auto & component : m_Components)
            component.second->setActive(value);
    }
private:
    ////////////////////////////////////////////////////////////
    /// Members
    ////////////////////////////////////////////////////////////
    bool m_Active;
    EntityKey m_ID;
    ComponentManager &m_Owner;
    ComponentPointers m_Components;
};

} // namespace Ghrum

#endif // _COMPONENT_HOLDER_HPP_