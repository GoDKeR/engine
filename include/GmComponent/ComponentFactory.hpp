/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMPONENT_FACTORY_HPP_
#define _COMPONENT_FACTORY_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "ComponentHolder.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a factory for registering signatures.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ComponentFactory {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Process an entity with default values.
    ///
    /// \param[in] entity The entity to process
    ////////////////////////////////////////////////////////////
    virtual void process(ComponentHolder &entity) const = 0;
};

////////////////////////////////////////////////////////////
/// \brief Detail class for implementing any factory.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
template<class ...T>
class ComponentFactoryDetail : public ComponentFactory {
public:
    ////////////////////////////////////////////////////////////
    /// \copydoc ComponentFactory::process(ComponentHolder&)
    ////////////////////////////////////////////////////////////
    void process(ComponentHolder &entity) const override {
        processEntity<T...>(entity);
    }
private:
    ////////////////////////////////////////////////////////////
    /// \brief Process an entity with default values.
    ///
    /// \tparam T1 Template of type Component
    /// \tparam T2 Template of type Component
    /// \tparam T3 Template of type Component
    ///
    /// \param[in] entity The entity to process
    ////////////////////////////////////////////////////////////
    template <class T1, enableIfBaseOf(Component, T1), class T2, class ...T3>
    void processEntity(ComponentHolder &entity) const {
        entity.addComponent<T1>();
        processEntity<T2, T3...>();
    }
};

} // namespace Ghrum

#endif // _COMPONENT_FACTORY_HPP_