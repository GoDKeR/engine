/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMPONENT_COLLECTION_HPP_
#define _COMPONENT_COLLECTION_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "Component.hpp"
#include <vector>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define an interface to hold arbitrary number of
///        components.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
template<class T, enableIfBaseOf(Component, T)>
class ComponentCollection {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Adds a component into the array.
    ///
    /// \tparam J Template of type Any
    ///
    /// \param[in] parameter The list of parameters
    ///
    /// \return The newly created component
    ////////////////////////////////////////////////////////////
    template<typename ...J>
    T &addComponent(J &&... parameter) {
        m_Array.emplace_back(std::forward<J>(parameter)...);
        return m_Array.back();
    }

    ////////////////////////////////////////////////////////////
    /// \brief Gets the list of registered components.
    ///
    /// \return The list of all registered components
    ////////////////////////////////////////////////////////////
    const std::vector<T> &getComponents() const {
        return m_Array;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Dispose all components
    ////////////////////////////////////////////////////////////
    void dispose() {
        m_Array.clear();
    }
private:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::vector<T> m_Array;
};

} // namespace Ghrum

#endif // _COMPONENT_COLLECTION_HPP_