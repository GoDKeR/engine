/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMPONENT_FILTER_HPP_
#define _COMPONENT_FILTER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "ComponentHolder.hpp"
#include <set>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a filter of components.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ComponentFilter {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default empty constructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_CTOR(ComponentFilter);

    ////////////////////////////////////////////////////////////
    /// \brief Default copy constructor and assigment.
    ////////////////////////////////////////////////////////////
    DEFAULT_COPY_CTOR(ComponentFilter);
    DEFAULT_ASSIGMENT(ComponentFilter);

    ////////////////////////////////////////////////////////////
    /// \brief Adds a component into the filter.
    ///
    /// \tparam T1 Template of type Component
    /// \tparam T2 Template of type Component
    /// \tparam T3 Template of type Component
    ///
    /// \return The instance of this filter
    ////////////////////////////////////////////////////////////
    template <class T1, enableIfBaseOf(Component, T1), class T2, class ...T3>
    ComponentFilter &isIncluded() {
        m_Include.emplace_back(T1::TYPE_ID);
        isIncluded<T2, T3...>();
        return *this;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Check if the holder can be process by this filter.
    ///
    /// \param[in] holder The holder to process
    ///
    /// \return True if the holder can be processed
    ////////////////////////////////////////////////////////////
    inline bool canProcess(const ComponentHolder &holder) const {
        for (const auto & key : m_Include) {
            if (!holder.hasComponent(key))
                return false;
        }
        return true;
    }
private:
    ////////////////////////////////////////////////////////////
    /// Members
    ////////////////////////////////////////////////////////////
    std::vector<ComponentKey> m_Include;
};

} // namespace Ghrum

#endif // _COMPONENT_FILTER_HPP_