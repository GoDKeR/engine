/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMPONENT_MANAGER_HPP_
#define _COMPONENT_MANAGER_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "ComponentCollection.hpp"
#include <unordered_map>
#include <memory>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the manager of components.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ComponentManager {
private:
    ////////////////////////////////////////////////////////////
    /// Typedef
    ////////////////////////////////////////////////////////////
    typedef ComponentCollection<Component> Collection;
    typedef std::unique_ptr<Collection>    CollectionPtr;
public:
    ////////////////////////////////////////////////////////////
    /// \brief Creates a new component from a parameter array.
    ///
    /// \tparam T Template of type Component
    /// \tparam J template of type Any
    ///
    /// \param[in] parameters The list of parameters
    ///
    /// \return The newly component created
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T), typename ...J>
    inline T &addComponent(J... parameters) {
        const auto &pIterator = m_Collections.find(T::TYPE_ID);
        if (pIterator != std::end(m_Collections))
            pIterator->second->addComponent(std::forward(parameters)...);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Register a new type of component.
    ///
    /// \tparam T Template of type Component
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T)>
    inline void registerComponent() {
        const auto &pIterator = m_Collections.find(T::TYPE_ID);
        if (pIterator == std::end(m_Collections))
            m_Collections.insert(T::TYPE_ID, buildCollection<T>());
    }

    ////////////////////////////////////////////////////////////
    /// \brief Unregister a type of component.
    ///
    /// \tparam T Template of type Component
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T)>
    inline void unregisterComponent() {
        const auto &pIterator = m_Collections.find(T::TYPE_ID);
        if (pIterator != std::end(m_Collections))
            m_Collections.erase(pIterator);
    }
private:
    ////////////////////////////////////////////////////////////
    /// \brief Builds a new collection.
    ///
    /// \tparam T Template of type Component
    ////////////////////////////////////////////////////////////
    template<class T, enableIfBaseOf(Component, T)>
    CollectionPtr buildCollection() {
        return CollectionPtr(
                   static_cast<Collection *>(new ComponentCollection<T>()));
    }
private:
    ////////////////////////////////////////////////////////////
    /// Members
    ////////////////////////////////////////////////////////////
    std::unordered_map<ComponentKey, std::unique_ptr<Collection>> m_Collections;
};

} // namespace Ghrum

#endif // _COMPONENT_MANAGER_HPP_