/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMPONENT_EXECUTOR_HPP_
#define _COMPONENT_EXECUTOR_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "ComponentFilter.hpp"

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define a system that logic every component.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ComponentExecutor {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ////////////////////////////////////////////////////////////
    ComponentExecutor()
        : m_Enabled(true) {
    }

    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(ComponentExecutor);

    ////////////////////////////////////////////////////////////
    /// \brief Return if the system is enabled.
    ///
    /// \return True if the system is enabled
    ////////////////////////////////////////////////////////////
    inline bool isEnabled() const {
        return m_Enabled;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Set enabled flag of the system.
    ///
    /// \param[in] value Boolean flag value
    ////////////////////////////////////////////////////////////
    inline void setEnabled(bool value) {
        m_Enabled = value;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Adds a holder into the system.
    ///
    /// \param[in] holder The holder to add into the system
    ////////////////////////////////////////////////////////////
    inline void registerEntity(ComponentHolder &holder) {
        m_Holders.push_back(&holder);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Process every entity registered
    ////////////////////////////////////////////////////////////
    virtual void processEntities() const {
        for (const auto & entity : m_Holders)
            processEntity(*entity);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Return if the system can be processed.
    ///
    /// \param[in] tick The delta tick time
    ///
    /// \return True if the system can be processed
    ////////////////////////////////////////////////////////////
    virtual bool isAvailable(Tick tick) const {
        return true;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Check if the holder can be processed by this system
    ///
    /// \param[in] holder The holder to check
    ///
    /// \return True if the holder can be processed by this system
    ////////////////////////////////////////////////////////////
    virtual bool isValid(const ComponentHolder &holder) const {
        return false;
    }

    ////////////////////////////////////////////////////////////
    /// \brief Process the entity selected.
    ///
    /// \param[in] holder The holder to process
    ////////////////////////////////////////////////////////////
    virtual void processEntity(ComponentHolder &holder) const {
    }
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    bool m_Enabled;
    std::vector<ComponentHolder *> m_Holders;
};


////////////////////////////////////////////////////////////
/// \brief Detail implementation of a component executor.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
template<class ...T>
class ComponentExecutorDetail : public ComponentExecutor {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default constructor.
    ////////////////////////////////////////////////////////////
    ComponentExecutorDetail() {
        m_Filter.isIncluded<T...>();
    }

    ////////////////////////////////////////////////////////////
    /// \copydoc ComponentExecutor::isValid(const ComponentHolder&)
    ////////////////////////////////////////////////////////////
    bool isValid(const ComponentHolder &holder) const override {
        return m_Filter.canProcess(holder);
    }

    ////////////////////////////////////////////////////////////
    /// \copydoc ComponentExecutor::processEntity(ComponentHolder&)
    ////////////////////////////////////////////////////////////
    void processEntity(ComponentHolder &holder) const override {
        processEntity(holder, holder.getComponent<T>()...);
    }

    ////////////////////////////////////////////////////////////
    /// \brief Process the entity selected with the components
    ///
    /// \param[in] holder    The holder to process
    /// \param[in] arguments All the components of the executor
    ////////////////////////////////////////////////////////////
    virtual void processEntity(ComponentHolder &holder, T... arguments) const = 0;
protected:
    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    ComponentFilter m_Filter;
};

} // namespace Ghrum

#endif // _COMPONENT_EXECUTOR_HPP_