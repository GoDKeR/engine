/*
 * Copyright (c) 2013 Ghrum Engine.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _COMPONENT_WORLD_HPP_
#define _COMPONENT_WORLD_HPP_

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include "ComponentExecutor.hpp"
#include "ComponentFactory.hpp"
#include <memory>

namespace Ghrum {

////////////////////////////////////////////////////////////
/// \brief Define the world manager of every component and
///        entity.
///
/// \author Agustin L. Alvarez <wolftein@ghrum.org>
////////////////////////////////////////////////////////////
class ComponentWorld : public ComponentManager {
public:
    ////////////////////////////////////////////////////////////
    /// \brief Default destructor.
    ////////////////////////////////////////////////////////////
    DEFAULT_DTOR(ComponentWorld);

    ////////////////////////////////////////////////////////////
    /// \brief Adds a system into the list.
    ///
    /// \param[in] name   The name of the system
    /// \param[in] system The system to register
    ////////////////////////////////////////////////////////////
    virtual void registerExecutor(const std::string &name,
                                  std::unique_ptr<ComponentExecutor> system) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Removes a system from the list.
    ///
    /// \param[in] name The name of the system
    ////////////////////////////////////////////////////////////
    virtual void unregisterExecutor(const std::string &name) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Gets a system by its name.
    ///
    /// \param[in] name The name of the system
    ///
    /// \return The system pointer
    ////////////////////////////////////////////////////////////
    virtual ComponentExecutor *const getExecutor(const std::string &name) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Register a new entity factory.
    ///
    /// \param[in] id      The name of the factory
    /// \param[in] factory The factory instance
    ////////////////////////////////////////////////////////////
    virtual void registerFactory(const std::string &id,
                                 std::unique_ptr<ComponentFactory> factory) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Removes an entity factory.
    ///
    /// \param[in] id The name of the factory to remove
    ////////////////////////////////////////////////////////////
    virtual void unregisterFactory(const std::string &id) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Creates a new entity in the world.
    ///
    /// \return The newly created entity
    ////////////////////////////////////////////////////////////
    virtual ComponentHolder &createEntity() = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Creates a new entity in the world using a prefab
    ///        entity factory.
    ///
    /// \param[in] id The id of the entity's factory
    ///
    /// \return The newly created entity
    ////////////////////////////////////////////////////////////
    virtual ComponentHolder &createEntity(const std::string &id) = 0;

    ////////////////////////////////////////////////////////////
    /// \brief Updates every system.
    ///
    /// \param[in] tick The current tick
    ////////////////////////////////////////////////////////////
    virtual void update(Tick tick) = 0;
};

} // namespace Ghrum

#endif // _ENTITY_WORLD_HPP_